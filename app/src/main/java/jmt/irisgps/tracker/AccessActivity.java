package jmt.irisgps.tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import jmt.irisgps.tracker.http_service.task.StartConnectionTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.TasksDA;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class AccessActivity extends ActionBarActivity {
    EditText txt_dni;
    EditText txt_placa;
    AccessAccountE user;
    Button btn_accept;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access);
        btn_accept = (Button)findViewById(R.id.btn_accept);
        txt_dni = (EditText)findViewById(R.id.txt_dni);
        txt_placa = (EditText)findViewById(R.id.txt_placa);
        txt_dni.setText("");
        txt_placa.setText("");
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });
    }
    public void send(){
        String dni = txt_dni.getText().toString();
        String placa = txt_placa.getText().toString();
        btn_accept.setEnabled(false);
        txt_placa.setEnabled(false);
        txt_dni.setEnabled(false);
        if(dni.length()>0&&placa.length()>0){
            user = new AccessAccountE(placa,dni);
            StartConnectionTask saTask = new StartConnectionTask(this,new CallbackRequest() {
                @Override
                public void processFinish() {
                    btn_accept.setEnabled(true);
                    txt_placa.setEnabled(true);
                    txt_dni.setEnabled(true);
                }
                @Override
                public void processFinish(ResponseE response) {
                    btn_accept.setEnabled(true);
                    txt_placa.setEnabled(true);
                    txt_dni.setEnabled(true);
                    onFinishRequest(response);
                }
            });
            saTask.execute(new Gson().toJson(user));
        }
    }
    public void onFinishRequest(ResponseE response){
        if(response.isSuccess()&&response.getCode().equals(S.RESPONSE.irs_success)){
            try {
                txt_dni.setEnabled(false);
                txt_placa.setEnabled(false);
                JSONObject jo = new JSONObject(response.getData());
                user.setTracking_route_id(jo.getString(S.ACCESS_ACCOUNT.id));
                user.setVehicle_id(jo.getInt(S.ACCESS_ACCOUNT.vehicle_id));
                new UserAccountDA().insert(user);

                if(!jo.isNull(S.RESULT.tasks))
                    new TasksDA().insert(jo.getString(S.RESULT.tasks));
                else
                    new TasksDA().deleteAll();

                startActivity(new Intent(AccessActivity.this,HomeMapActivity.class));
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(response.isSuccess()&&response.getCode().equals(S.RESPONSE.irs_invalid_data)){
//            Toast.makeText(this,getString(R.string.s_datos_invalidos),Toast.LENGTH_LONG).show();
            Toast.makeText(this,response.getMassege(),Toast.LENGTH_LONG).show();
        }else if(!response.isSuccess()){
//            Toast.makeText(this,getString(R.string.s_sin_conexion),Toast.LENGTH_LONG).show();
            Toast.makeText(this,response.getMassege(),Toast.LENGTH_LONG).show();
        }
    }
}