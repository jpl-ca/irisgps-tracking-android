package jmt.irisgps.tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jmt.irisgps.tracker.dialog.MessageDialog;
import jmt.irisgps.tracker.dialog.RegisterIncidentDialog;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.data_access.TasksDA;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.CustomerE;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.service.LocationFusedService;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class HomeMapActivityDep extends ActionBarActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{
    private GoogleMap map;
    Toolbar toolbar;
    Util util;
    Marker marker[];
    Marker markerME;
    HashMap<String,Integer> HashCli;
    Intent intentGPS,intentFused;
    TasksDA tasksDA;
    List<TasksE> taskList;
    GpsReceiver myReceiver;
    PolylineOptions options,OptionsLineBlue,OptionsLineGreen;
    Polyline lineMe,lineBlue,lineGreen;
    int REQUEST_CODE = 10101;
    LatLng ME;
    AccessAccountE vehicle;
    Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_bar);
        util = new Util(this);
        gson = new Gson();
        tasksDA = new TasksDA();
        vehicle = new UserAccountDA().select();
        getSupportActionBar().setTitle(vehicle.getPlate());
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        options = new PolylineOptions().width(4).color(Color.parseColor("#e74c3c")).geodesic(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        ME = new LatLng(0,0);
        LocationHistoriesE lastLocation = new RoutePositionDA().last();
        if(lastLocation!=null){
            ME = new LatLng(lastLocation.getLatitud(),lastLocation.getLongitud());
            MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
            markerME = map.addMarker(mo);
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 11));
        map.animateCamera(CameraUpdateFactory.zoomTo(11), 500, null);
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });
        addCustomerPlaces(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        intentFused = new Intent(this, LocationFusedService.class);
        startService(intentFused);
//        intentGPS = new Intent(this, LocationGPSService.class);
//        startService(intentGPS);
        myReceiver = new GpsReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LocationFusedService.NEW_GPS);
        registerReceiver(myReceiver, intentFilter);
        turnGPSOn();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addCustomerPlaces(false);
    }

    private void addCustomerPlaces(boolean first_time){
        final LatLngBounds.Builder builderLL = new LatLngBounds.Builder();
        HashCli = new HashMap<>();
        taskList = tasksDA.select();
        if(marker!=null)for (int i=0;i<marker.length;i++)marker[i].remove();
        marker = new Marker[taskList.size()];
        System.out.println("In Route?"+vehicle.getTracking_route_id());
        boolean IS_FINISH=true;
        for (int i=0;i<taskList.size();i++){
            long task_st_id = taskList.get(i).getTask_state_id();
            if(task_st_id==S.TASK_STATE.PROGRAMADA||task_st_id==S.TASK_STATE.POSPUESTA)
                IS_FINISH = false;
            CustomerE cli = taskList.get(i).getCustomer();
            int ic_loc = 0;
            int task_state = (int)taskList.get(i).getTask_state_id();
            if(task_state==S.TASK_STATE.PROGRAMADA)
                ic_loc = R.drawable.ic_place_purple;
            else if(task_state==S.TASK_STATE.REALIZADA)
                ic_loc = R.drawable.ic_place_green;
            else if(task_state==S.TASK_STATE.POSPUESTA)
                ic_loc = R.drawable.ic_place_orange;
            else if(task_state==S.TASK_STATE.CANCELADA)
                ic_loc = R.drawable.ic_place_gray;
            else
                ic_loc = R.drawable.ic_pistachio;
            LatLng ll = new LatLng(cli.getLat(),cli.getLng());
            MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory
                    .fromResource(ic_loc)).position(ll).title(cli.getName());
            marker[i] = map.addMarker(mo);
            HashCli.put(marker[i].getId(),i);
            builderLL.include(ll);
        }
        if(first_time&&taskList.size()>0)
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition arg0) {
//                    builderLL.include(ME);
//                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builderLL.build(), 100));
//                    map.setOnCameraChangeListener(null);
                }
            });
        AccessAccountE user = new UserAccountDA().select();
        System.out.println("IS FINISH??"+IS_FINISH);
        user.setRoutes_completed(IS_FINISH);
        user.save();
        map.setOnMarkerClickListener(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_registro_incidente) {
            registroIncidente();
            return true;
        }
        if (id == R.id.action_desconectar) {
//            stopService(intentGPS);
            util.finalizarSesion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void registroIncidente() {
        new RegisterIncidentDialog(this).show(this,ME,new CallbackDialog() {
            @Override
            public void finish() {
                System.out.println("This a greeting!!");
            }
        });
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        if(HashCli.containsKey(marker.getId())){
            Intent it= new Intent(this,TaskCustomerActivity.class);
            TasksE task = taskList.get(HashCli.get(marker.getId()));
            it.putExtra(S.TASK.id,task.getId());
            it.putExtra(S.TASK.route_task_id,task.getTask_id());
            startActivity(it);
        }
        return false;
    }

    String rr = "";
    private class GpsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            System.out.println("New Position!");
            /*
                TODO MOSTRAR LINEA PARA EFECTO DE PRUEBA
                TODO MOSTRAR LINEA PARA EFECTO DE PRUEBA
                TODO MOSTRAR LINEA PARA EFECTO DE PRUEBA
                TODO MOSTRAR LINEA PARA EFECTO DE PRUEBA
                TODO MOSTRAR LINEA PARA EFECTO DE PRUEBA
             */
            boolean SHOWLINE = true;
            Bundle extra = arg1.getExtras();
            if(extra!=null){
                int type_msg = extra.getInt(S.TYPE_MSG.type_msg);
                if(type_msg==S.TYPE_MSG.FIRST_POSITION){
                    Double Lat = extra.getDouble(S.POSITION.latitude);
                    Double Lng = extra.getDouble(S.POSITION.longitude);
                    boolean isFirst = ME.latitude == 0 || ME.longitude == 0 ;
                    if(isFirst){
                        ME = new LatLng(Lat,Lng);
                        if(markerME!=null) markerME.remove();
                        MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
                        markerME = map.addMarker(mo);
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 15));
                        map.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);
                    }
                    return;
                }

                if(type_msg==S.TYPE_MSG.ADD_POSITION){
                    Double Lat = extra.getDouble(S.POSITION.latitude);
                    Double Lng = extra.getDouble(S.POSITION.longitude);
                    rr += "new LatLng("+Lat+","+Lng+"),";
                    System.out.println("El Arr ess:"+rr);
                    boolean isFirst = ME.latitude == 0 || ME.longitude == 0 ;
                    ME =  new LatLng(Lat,Lng);
                    if(markerME!=null) markerME.remove();
                    MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
                    markerME = map.addMarker(mo);
                    if(isFirst){
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 15));
                        map.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);
                    }

                    if(SHOWLINE){
                        float speed = extra.getFloat("speed");
                        float accuracy = extra.getFloat("accuracy");
                        double speedKM = extra.getDouble("speedKM");
                        String provider = extra.getString("provider");
                        String moving = extra.getString("moving");
                        String tit = Lat+","+Lng;
                        ((TextView)findViewById(R.id.txtLL)).setText(tit);
                        ((TextView)findViewById(R.id.txtLL3)).setText(moving+"  |  "+speed+" | "+speedKM);
                        if(lineMe!=null) lineMe.remove();
                        options.add(ME);
//                        lineMe = map.addPolyline(options);
                    }

                    if(SHOWLINE){
//                        String fix = extra.getString("fix");
//                        System.out.println("Llego Fix:"+fix);
                        String sin_fix = extra.getString("sin_fix");
//                        ArrayList<LatLng> locs= gson.fromJson(fix, new TypeToken<ArrayList<LatLng>>(){}.getType());
                        ArrayList<LatLng> locsG= gson.fromJson(sin_fix, new TypeToken<ArrayList<LatLng>>(){}.getType());
//                        OptionsLineBlue = new PolylineOptions().width(2).color(Color.BLUE).geodesic(true);
                        OptionsLineGreen = new PolylineOptions().width(5).color(Color.GREEN).geodesic(true);
//                        if(lineBlue!=null) lineBlue.remove();
                        if(lineGreen!=null) lineGreen.remove();
//                        System.out.println("Llego con sz:"+locs.size());
//                        for (LatLng ll : locs)
//                            OptionsLineBlue.add(ll);
                        for (LatLng ll : locsG)
                            OptionsLineGreen.add(ll);
                        lineGreen = map.addPolyline(OptionsLineGreen);
//                        lineBlue = map.addPolyline(OptionsLineBlue);
                    }
                }else{
                    alertTurnGPS();
                }
            }
        }
    }


    private void turnGPSOn(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){
            alertTurnGPS();
        }
    }
    private void alertTurnGPS(){
        new MessageDialog(HomeMapActivityDep.this).show(getString(R.string.s_activar_gps), new CallbackDialog() {
            @Override
            public void finish() {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
                if(intentGPS!=null)
                startService(intentGPS);
            }else{
                alertTurnGPS();
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }
}