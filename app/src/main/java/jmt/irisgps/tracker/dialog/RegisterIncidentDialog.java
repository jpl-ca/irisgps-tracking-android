package jmt.irisgps.tracker.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import jmt.irisgps.tracker.R;
import jmt.irisgps.tracker.http_service.task.SendBatchLocationTask;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.ResponseE;
//import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by JMTech-Android on 22/05/2015.
 */
public class RegisterIncidentDialog extends MaterialDialog{
    Context ctx;
    EditText txt_incidencia;
    int IncidentTypeSelected;
    Spinner spinner;
    InputMethodManager imm;
    public RegisterIncidentDialog(Context context) {
        super(context);
        LayoutInflater factory = LayoutInflater.from(context);
        View myView = factory.inflate(R.layout.dialog_registrer_incident, null);
        setContentView(myView);
        txt_incidencia = (EditText)myView.findViewById(R.id.txt_incidencia);
        spinner = (Spinner) myView.findViewById(R.id.sp_state);
        setTitle(context.getString(R.string.s_registrar_incidente));
        ctx = context;
        IncidentTypeSelected = 0;

        final String[] items = ctx.getResources().getStringArray(R.array.tipo_incidente);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
    }
    public void show(final Activity activity,final LatLng position, final CallbackDialog _callback){
        final RoutePositionDA route_pos = new RoutePositionDA();
        setPositiveButton(" "+ctx.getString(R.string.enviar).toUpperCase(),new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String incidencia = txt_incidencia.getText().toString();
                IncidentTypeSelected = spinner.getSelectedItemPosition()+1;

                if(incidencia.length()>3){
                    txt_incidencia.setEnabled(false);
                    imm.hideSoftInputFromWindow(txt_incidencia.getWindowToken(), 0);
                    route_pos.insert(new LocationHistoriesE(position.latitude, position.longitude, IncidentTypeSelected, incidencia));
                    SendBatchLocationTask batchTask = new SendBatchLocationTask(activity,new CallbackRequest() {
                        @Override
                        public void processFinish() {
//                            Toast.makeText(activity,activity.getString(R.string.s_error_envio),Toast.LENGTH_LONG).show();
                            _callback.finish();
                            dismiss();
                        }
                        @Override
                        public void processFinish(ResponseE response) {
                            txt_incidencia.setEnabled(true);
                            _callback.finish();
                            dismiss();
                        }
                    });
                    batchTask.execute();
                }else{
                    Toast.makeText(ctx,ctx.getString(R.string.s_descripcion_muy_corta),Toast.LENGTH_LONG).show();
                }
            }
        });
        setNegativeButton(ctx.getString(R.string.cancelar).toUpperCase()+" ",new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(txt_incidencia.getWindowToken(), 0);
                dismiss();
            }
        });
        show();
    }
}