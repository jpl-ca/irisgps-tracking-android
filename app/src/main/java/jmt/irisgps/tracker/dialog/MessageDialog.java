package jmt.irisgps.tracker.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import jmt.irisgps.tracker.R;
import jmt.irisgps.tracker.interfaz.CallbackDialog;

/**
 * Created by JMTech-Android on 22/05/2015.
 */
public class MessageDialog extends MaterialDialog{
    Context ctx;
    TextView tv_m;
    public MessageDialog(Context context) {
        super(context);
        LayoutInflater factory = LayoutInflater.from(context);
        View myView = factory.inflate(R.layout.dialog_msg, null);
        setContentView(myView);
        setTitle(context.getString(R.string.importante));
        tv_m = (TextView) myView.findViewById(R.id.txt_message);
        ctx = context;
    }
    public void show(final String msg,final CallbackDialog _delegate){
        tv_m.setText(msg);
        setPositiveButton(ctx.getString(R.string.s_CONFIGURAR), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                _delegate.finish();
            }
        });
        show();
    }
}