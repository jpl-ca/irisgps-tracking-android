package jmt.irisgps.tracker.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Date;

import jmt.irisgps.tracker.http_service.task.SendBatchLocationTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation,mPreLocation,mValidLocation;
    protected double velocidadActual,velocidadAnterior,velocidadPreAnterior;

    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;
    private long LAST_TIME_UPDATED = 0,LAST_TIME_READ = 0;
    Gson gson;
    Util util;
    boolean IS_STOPPED;
    Jmstore jmstore;
    RoutePositionDA positionDA;
    int totalLocations;
    ArrayList<LatLng> ListaPosicion;
    Vibrator v;
    @Override
    public void onCreate() {
        super.onCreate();
        mCurrentLocation=mPreLocation=null;
        velocidadActual=velocidadAnterior=velocidadPreAnterior=0;
        mRequestingLocationUpdates = false;
        intent = new Intent();
        positionDA = new RoutePositionDA();
        totalLocations = 0;
        ListaPosicion = new ArrayList<>();
        gson = new Gson();
        jmstore = new Jmstore(this);
        util = new Util(this);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        IS_STOPPED = false;
        LAST_TIME_READ = new Date().getTime();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        sendBroadcast(intent);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            showMapPosition(mCurrentLocation);
        }
        startLocationUpdates();
    }

    private void showMapPosition(Location location) {
        intent.putExtra(S.TYPE_MSG.type_msg, S.TYPE_MSG.FIRST_POSITION);
        intent.putExtra(S.POSITION.latitude,location.getLatitude());
        intent.putExtra(S.POSITION.longitude,location.getLongitude());
        intent.setAction(NEW_GPS);
        sendBroadcast(intent);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        velocidadActual = mCurrentLocation.getSpeed();
        System.out.println("-)>"+mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude()+"  SPEED:"+location.getSpeed());
        if(mValidLocation!=null&&((location.getProvider().equals("fused")||location.getProvider().equals("gps"))&&mCurrentLocation.getAccuracy()<50)){
            if(speedInvalid()){
                revisarEstadoDetenido();
                String contactOpenStr = getContactVehicleState();
                String contactOpenS = contactOpenStr.equals("0")?"Des":"Con";
                String saveInFile = mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude() + ","+ mCurrentLocation.getSpeed() + ","+getKmByHour(mCurrentLocation.getSpeed())+","+Util.now()+",***,"+contactOpenS;
                util.saveInfoGps(saveInFile,"all_gps");
            }else if(samePosition(mCurrentLocation, mPreLocation)||samePosition(mCurrentLocation, mValidLocation)){
                revisarEstadoDetenido();
            }else{
                IS_STOPPED = false;
                mValidLocation = mCurrentLocation;
                if(ListaPosicion.size()>0){
                    int px = ListaPosicion.size()-1;
                    double lastDist =  calcDistance(ListaPosicion.get(px).latitude,ListaPosicion.get(px).longitude,mValidLocation.getLatitude(),mValidLocation.getLongitude());
                    double KmxHr = getKmByHour(lastDist);
                    System.out.println("TWO DIST ES:"+lastDist+"  & VEL:"+KmxHr);
                    if(KmxHr<180)
                        updateUI(true);
                }
            }
        }else{
            showMapPosition(location);
            mValidLocation = mCurrentLocation;
            System.out.println("Bloqueo,Bloqueo,Bloqueo,Bloqueo!!!");
        }
        mPreLocation = mCurrentLocation;
    }
    public void revisarEstadoDetenido(){
        if(!IS_STOPPED){
            mValidLocation = mPreLocation;
            updateUI(false);
        }
        IS_STOPPED = true;
        mValidLocation = mCurrentLocation;
    }
    private double getKmByHour(double spd) {
        return ((spd*3600)/1000);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void updateUI(boolean isMoving) {
        if (mValidLocation != null) {
            if(!isMoving) v.vibrate(250);
            String estado_vehiculo = isMoving?"En Movimiento":"Detenido";
            ListaPosicion.add(new LatLng(mValidLocation.getLatitude(),mValidLocation.getLongitude()));
            /**  Send new Position **/
            intent.putExtra(S.TYPE_MSG.type_msg, S.TYPE_MSG.ADD_POSITION);
            intent.putExtra(S.POSITION.latitude,mValidLocation.getLatitude());
            intent.putExtra(S.POSITION.longitude,mValidLocation.getLongitude());
            intent.putExtra("accuracy",mValidLocation.getAccuracy());
            intent.putExtra("speed",mValidLocation.getSpeed());
            intent.putExtra("speedKM",getKmByHour(mValidLocation.getSpeed()));
            intent.putExtra("provider", mValidLocation.getProvider());
            intent.putExtra("totalLocations", totalLocations);
            intent.putExtra("sin_fix", gson.toJson(ListaPosicion));
            intent.putExtra("moving", estado_vehiculo);
            intent.setAction(NEW_GPS);
            sendBroadcast(intent);

            /**  * REGISTRAR EN LA BD LOCAL **/
            long CURRENT_TIME = new Date().getTime();
            double speedKM = getKmByHour(mValidLocation.getSpeed());
            String contactOpenStr = getContactVehicleState();
            int contactOpen = (contactOpenStr.equals("0")?0:1);
            int movement = isMoving?1:0;
            String contactOpenS = contactOpen==0?"Des":"Con";
            String movementS = isMoving?"Mov":"Det";
            totalLocations = positionDA.insert(new LocationHistoriesE(mValidLocation.getLatitude(), mValidLocation.getLongitude(),mValidLocation.getSpeed(),speedKM,movement,contactOpen));
            String saveInFile = mValidLocation.getLatitude()+","+mValidLocation.getLongitude() + ","+ mValidLocation.getSpeed() + ","+speedKM+","+Util.now()+","+movementS+","+contactOpenS;
            util.saveInfoGps(saveInFile,"all_gps");
            util.saveInfoGps(saveInFile,"filter");

            /** *ENVIAR AUTOMATICAMENTE,SI COMO MINIMO HAY 7 DATOS REGISTRADOS EN LA BD LOCAL O ES EL PRIMER REGISTRO */
            System.out.println("Total Locations:"+totalLocations);
            LAST_TIME_READ = CURRENT_TIME;
            /**  TODO ANTES ERA 10mn 'LAST_TIME_UPDATED' AHORA 3mn  */
            if(totalLocations>7||(CURRENT_TIME-LAST_TIME_UPDATED>3*60*1000&&totalLocations>1)){
                LAST_TIME_UPDATED = CURRENT_TIME;
                new SendBatchLocationTask(this,new CallbackRequest() {
                    @Override
                    public void processFinish() {}
                    @Override
                    public void processFinish(ResponseE response) {
                        System.out.println("Enviado.....!!!!");
                    }
                }).execute();
            }
        }
    }

    private String getContactVehicleState() {
        String contactOpenStr = jmstore.get(S.CONTACT_OPEN);
        if(contactOpenStr.equals("")){
            contactOpenStr=Util.isConnected(this)?"1":"0";
            jmstore.push(S.CONTACT_OPEN,contactOpenStr);
        }
        return contactOpenStr;
    }

    public boolean speedInvalid() {
        boolean b = false;
        double SPEED_ALLOW = 1;
        if(velocidadActual==0.0||(velocidadActual<SPEED_ALLOW&&velocidadAnterior<SPEED_ALLOW)||(velocidadActual<SPEED_ALLOW&&velocidadPreAnterior<SPEED_ALLOW))
            b = true;
        velocidadPreAnterior = velocidadAnterior;
        velocidadAnterior = velocidadActual;
        return b;
    }
    public boolean samePosition(Location ll1, Location ll2) {
        return ll1.getLatitude()==ll2.getLatitude()&&ll1.getLongitude()==ll2.getLongitude();
    }
    public double calcDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }
}