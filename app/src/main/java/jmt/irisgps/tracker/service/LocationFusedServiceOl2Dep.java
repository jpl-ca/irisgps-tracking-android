package jmt.irisgps.tracker.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedServiceOl2Dep extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    public static final int MIN_DISTANCE_DISPLACEMENT = 10;
//    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation,mPreLocation,mValidLocation;
    protected double velocidadActual,velocidadAnterior,velocidadPreAnterior;

    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;
    private long LAST_TIME_UPDATED = 0,LAST_TIME_READ = 0;
    Gson gson;
    Util util;
    boolean IS_STOPPED;
    Jmstore jmstore;

    RoutePositionDA positionDA;
    int totalLocations;

    ArrayList<LatLng> ListaPosicion;

    @Override
    public void onCreate() {
        super.onCreate();
        mCurrentLocation=mPreLocation=null;
        velocidadActual=velocidadAnterior=velocidadPreAnterior=0;
        mRequestingLocationUpdates = false;
        intent = new Intent();
        positionDA = new RoutePositionDA();
        totalLocations = 0;
        ListaPosicion = new ArrayList<>();
        gson = new Gson();
        jmstore = new Jmstore(this);
        util = new Util(this);
        IS_STOPPED = false;
        LAST_TIME_READ = new Date().getTime();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        sendBroadcast(intent);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        System.out.println("Starting Update Position GPS!!!");
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        velocidadActual = mCurrentLocation.getSpeed();
        double distanciaLastPos = 0, distanciaLastPosValid = 0;
        if(mPreLocation!=null)distanciaLastPos = calcDistance(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(),mPreLocation.getLatitude(),mPreLocation.getLongitude());
        if(mValidLocation!=null)distanciaLastPosValid = calcDistance(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(),mValidLocation.getLatitude(),mValidLocation.getLongitude());
        System.out.println("-)>"+mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude()+"  DistL:"+String.format("%.3f", distanciaLastPos)+"  DistV:"+String.format("%.3f", distanciaLastPosValid)+"  SPEED:"+location.getSpeed());
        if(mValidLocation!=null&&((location.getProvider().equals("fused")||location.getProvider().equals("gps"))&&mCurrentLocation.getAccuracy()<50)){
            if(speedInvalid()){
                System.out.println("Velocidad Lenta...");
                if(!IS_STOPPED){
                    System.out.println("Vehiculo Detenido!!!");
                    mValidLocation = mPreLocation;
                    updateUI(false);
                }
                IS_STOPPED = true;
                mValidLocation = mCurrentLocation;
                sendPosition(S.TYPE_MSG.FIRST_POSITION,null);
                String contactOpenStr = getContactVehicleState();
                String contactOpenS = contactOpenStr.equals("0")?"Des":"Con";
                String saveInFile = mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude() + ","+ mCurrentLocation.getSpeed() + ","+getKmByHour(mCurrentLocation.getSpeed())+","+Util.now()+",***,"+contactOpenS;
                util.saveInfoGps(saveInFile,"all_gps");
            }else if(samePosition(mCurrentLocation, mPreLocation)||samePosition(mCurrentLocation, mValidLocation)){
                System.out.println("Misma Posición...");
                if(!IS_STOPPED){
                    System.out.println("Vehiculo Detenido!!!");
                    mValidLocation = mPreLocation;
                    updateUI(false);
                }
                IS_STOPPED = true;
                mValidLocation = mCurrentLocation;
                sendPosition(S.TYPE_MSG.FIRST_POSITION,null);
            }else{
                IS_STOPPED = false;
                if(distanciaLastPos>25){
                    System.out.println("-)>****************************************");
                    System.out.println("-)>"+distanciaLastPos);
                    System.out.println("-)>"+distanciaLastPosValid);
                    System.out.println("-)>****************************************");
                }
                mValidLocation = mCurrentLocation;
                updateUI(true);
            }
        }else{
            mValidLocation = mCurrentLocation;
            sendPosition(S.TYPE_MSG.FIRST_POSITION,null);
            System.out.println("Bloqueo,Bloqueo,Bloqueo,Bloqueo!!!");
        }
        mPreLocation = mCurrentLocation;
    }
    public void sendPosition(int type,String moviendo){
        intent.putExtra(S.TYPE_MSG.type_msg, type);
        intent.putExtra(S.POSITION.latitude,mValidLocation.getLatitude());
        intent.putExtra(S.POSITION.longitude,mValidLocation.getLongitude());
        intent.putExtra("accuracy",mValidLocation.getAccuracy());
        intent.putExtra("speed",mValidLocation.getSpeed());
        intent.putExtra("speedKM",getKmByHour(mValidLocation.getSpeed()));
        intent.putExtra("provider", mValidLocation.getProvider());
        intent.putExtra("totalLocations", totalLocations);
        intent.putExtra("sin_fix", gson.toJson(ListaPosicion));
        intent.putExtra("moving", moviendo==null?"":moviendo);
        intent.setAction(NEW_GPS);
        sendBroadcast(intent);
    }

    private double getKmByHour(double spd) {
        return ((spd*3600)/1000);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
    private void updateUI(boolean isMoving) {
        if (mValidLocation != null) {
            if(!isMoving){
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(750);
            }

            ListaPosicion.add(new LatLng(mValidLocation.getLatitude(),mValidLocation.getLongitude()));
            sendPosition(S.TYPE_MSG.ADD_POSITION,isMoving?"Movimiento":"Detenido");
            long CURRENT_TIME = new Date().getTime();
            double speedKM = getKmByHour(mValidLocation.getSpeed());
            /**
             * REGISTRAR EN LA BD LOCAL
             */
            String contactOpenStr = getContactVehicleState();
            int contactOpen = (contactOpenStr.equals("0")?0:1);
            int movement = isMoving?1:0;
            String contactOpenS = contactOpen==0?"Des":"Con";
            String movementS = isMoving?"Mov":"Det";
            totalLocations = positionDA.insert(new LocationHistoriesE(mValidLocation.getLatitude(), mValidLocation.getLongitude(),mValidLocation.getSpeed(),speedKM,movement,contactOpen));
            String saveInFile = mValidLocation.getLatitude()+","+mValidLocation.getLongitude() + ","+ mValidLocation.getSpeed() + ","+speedKM+","+Util.now()+","+movementS+","+contactOpenS;
            util.saveInfoGps(saveInFile,"all_gps");
            util.saveInfoGps(saveInFile,"filter");

            /**
             * ENVIAR AUTOMATICAMENTE,SI COMO MINIMO HAY 7 DATOS REGISTRADOS EN LA BD LOCAL O ES EL PRIMER REGISTRO
             */
            System.out.println("Total Locations:"+totalLocations);
            LAST_TIME_READ = CURRENT_TIME;
            /**
                TODO ANTES ERA 10mn 'LAST_TIME_UPDATED' AHORA 3mn
             */
//            if(totalLocations>7||CURRENT_TIME-LAST_TIME_UPDATED>3*60*1000){
//                LAST_TIME_UPDATED = CURRENT_TIME;
////                new SendBatchLocationTmpTask(this,new CallbackRequest() {
//                new SendBatchLocationTask(this,new CallbackRequest() {
//                    @Override
//                    public void processFinish() {}
//                    @Override
//                    public void processFinish(ResponseE response) {
//                        System.out.println("Enviado.....!!!!");
//                    }
//                }).execute();
//            }
        }
    }

    private String getContactVehicleState() {
        String contactOpenStr = jmstore.get(S.CONTACT_OPEN);
        if(contactOpenStr.equals("")){
            contactOpenStr=Util.isConnected(this)?"1":"0";
            jmstore.push(S.CONTACT_OPEN,contactOpenStr);
        }
        return contactOpenStr;
    }

//    public double velocidadAproximada(double lat1,double lng1, double lat2,double lng2, long secs){
//        double difDistance = 0;
//        long difTime = secs;
//        difDistance = calcDistance(lat1, lng1,lat2,lng2);
//        difTime = difTime==0?1:difTime;
//        String difDist = String.format("%.5f", difDistance/difTime);
//        difDist = difDist.replace(",",".");
//        return Double.parseDouble(difDist);
//    }

    public boolean speedInvalid() {
        boolean b = false;
        double SPEED_ALLOW = 1;
        if(velocidadActual==0.0||(velocidadActual<SPEED_ALLOW&&velocidadAnterior<SPEED_ALLOW)||(velocidadActual<SPEED_ALLOW&&velocidadPreAnterior<SPEED_ALLOW))
//        if((getKmByHour(velocidadActual)<SPEED_ALLOW&&getKmByHour(velocidadAnterior)<SPEED_ALLOW)||(getKmByHour(velocidadActual)<SPEED_ALLOW&&getKmByHour(velocidadPreAnterior)<SPEED_ALLOW))
            b = true;
        velocidadPreAnterior = velocidadAnterior;
        velocidadAnterior = velocidadActual;
        return b;
    }
    public boolean samePosition(Location ll1, Location ll2) {
        return ll1.getLatitude()==ll2.getLatitude()&&ll1.getLongitude()==ll2.getLongitude();
    }
    public double calcDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }
}