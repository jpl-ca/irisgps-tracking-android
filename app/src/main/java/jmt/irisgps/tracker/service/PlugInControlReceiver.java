package jmt.irisgps.tracker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;

/**
 * Created by JMTech-Android on 29/05/2015.
 */
public class PlugInControlReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        String action = intent.getAction();
        if(action.equals(Intent.ACTION_POWER_CONNECTED)) {
            new Jmstore(context).push(S.CONTACT_OPEN, "1");
        }else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
            new Jmstore(context).push(S.CONTACT_OPEN, "0");
        }
    }
}