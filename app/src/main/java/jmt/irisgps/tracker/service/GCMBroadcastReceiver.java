package jmt.irisgps.tracker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;

/**
 * Created by JMTech-Android on 29/05/2015.
 */
public class GCMBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        String regId = intent.getExtras().getString("registration_id");
        if(regId==null)return;
        new Jmstore(context).push(S.GCMID, regId);
    }
}