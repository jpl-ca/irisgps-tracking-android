package jmt.irisgps.tracker.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

/**
 * Created by JMTech-Android on 09/06/2015.
 */
public class LocationFusedServiceOld extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
//    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000/2;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final int UPDATE_DISTANCE_DISPLACEMENT = 5;
    public static final int MIN_DISTANCE_DISPLACEMENT = 10;
//    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    public final static String NEW_GPS = "NEW_GPS";
    protected Boolean mRequestingLocationUpdates;
    Intent intent;
    private long LAST_TIME_UPDATED = 0,LAST_TIME_READ = 0;
    Gson gson;
    Util util;
    boolean HARDATA;
    Jmstore jmstore;

    RoutePositionDA positionDA;
    int totalLocations;
    public final static int TOTAL_PRE_LOC = 3;
    LatLng preLocation[];

    ArrayList<LatLng> ListaPosicion;
    @Override
    public void onCreate() {
        super.onCreate();
        mRequestingLocationUpdates = false;
        intent = new Intent();
        positionDA = new RoutePositionDA();
        totalLocations = 0;
        preLocation = new LatLng[TOTAL_PRE_LOC];
        ListaPosicion = new ArrayList<>();
        gson = new Gson();
        jmstore = new Jmstore(this);
        util = new Util(this);
        HARDATA = false;
        LAST_TIME_READ = new Date().getTime();
        Arrays.fill(preLocation, new LatLng(0, 0));
        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setSmallestDisplacement(UPDATE_DISTANCE_DISPLACEMENT);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            mRequestingLocationUpdates = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
//        stopSelf();
//        /** onPause*/
//        if (mGoogleApiClient.isConnected()) {
//            stopLocationUpdates();
//        }
//        /** onStop*/
//        mGoogleApiClient.disconnect();
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        sendBroadcast(intent);
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        System.out.println("Starting Update Position GPS!!!");
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            updateUI();
        }
        startLocationUpdates();
    }
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        System.out.println("ACCURACY->"+mCurrentLocation.getAccuracy()+"  Prov:"+location.getProvider()+"  SPEED:"+location.getSpeed());
        if((location.getProvider().equals("fused")||location.getProvider().equals("gps"))&&mCurrentLocation.getAccuracy()<50){
            if(mCurrentLocation.getSpeed()>1.0)
                updateUI();
            else System.out.println("Slow..."+mCurrentLocation.getSpeed());
        }
        else System.out.println("Bloqueo,Bloqueo,Bloqueo,Bloqueo!!!");
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
    private void updateUI() {
        if (mCurrentLocation != null) {
            /**
             * ENVIAR A LA ACTIVIDAD PRINCIPAL PARA MOSTRAR AL USUARIO
             */
            intent.putExtra(S.TYPE_MSG.type_from, 1);
            intent.putExtra(S.POSITION.latitude,mCurrentLocation.getLatitude());
            intent.putExtra(S.POSITION.longitude,mCurrentLocation.getLongitude());
            intent.putExtra("accuracy",mCurrentLocation.getAccuracy());
            intent.putExtra("speed",mCurrentLocation.getSpeed());
            intent.putExtra("provider", mCurrentLocation.getProvider());
            intent.putExtra("totalLocations", totalLocations);
            intent.putExtra(S.TYPE_MSG.type_msg,S.TYPE_MSG.ADD_POSITION);
            intent.setAction(NEW_GPS);


            ListaPosicion.add(new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude()));
            if(HARDATA){
                if(ListaPosicion.size()>2){
                    sendBroadcast(intent);
                    return;
                }
            }
            /**
                Obtener velocidad aproximada
             */
            long CURRENT_TIME = new Date().getTime();
            double speedCustom = velocidadAproximada(CURRENT_TIME);

//            ArrayList<LatLng> ltLngs = fixTriangle(25);
            ArrayList<LatLng> ltLngs = removePeak();

            intent.putExtra("fix", gson.toJson(ltLngs));
            intent.putExtra("speed_aprox", speedCustom);
            intent.putExtra("sin_fix", gson.toJson(ListaPosicion));
            sendBroadcast(intent);

            String saveInFile = mCurrentLocation.getLatitude()+","+mCurrentLocation.getLongitude() + ","+ mCurrentLocation.getSpeed() + ","+speedCustom+","+Util.now()+","+mCurrentLocation.getProvider()+","+mCurrentLocation.getAccuracy();
//            util.saveInfoGps(saveInFile);

            System.out.println("Sent!!!!");
            /*
            TODO COMENTADO PARA EFECTOS DE PRUEBA
            TODO COMENTADO PARA EFECTOS DE PRUEBA
            TODO COMENTADO PARA EFECTOS DE PRUEBA
            TODO COMENTADO PARA EFECTOS DE PRUEBA
            TODO COMENTADO PARA EFECTOS DE PRUEBA
             */
//            if(true)return;
            /**
             * EVALUAR SI LA LOCALIZACION SE HA REPETIDO
             */
            for (int i=0;i<TOTAL_PRE_LOC;i++)
                if(preLocation[i].latitude==mCurrentLocation.getLatitude()&&preLocation[i].longitude==mCurrentLocation.getLongitude())return;

            /**
             * EVALUAR SI LA DISTANCIA ES VALIDAD PARA REGISTRAR O ESTA DEMASIADO CERCA PARA EVITAR REDUNDANCIAS
             * TODO COMENTADO PARA PROBAR
             */
//            boolean b = isDistanceValid(preLocation[0].latitude,preLocation[0].longitude,mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
//            if(!b)return;
            /**
             * REGISTRAR EN LA BD LOCAL
             */


            totalLocations = positionDA.insert(new LocationHistoriesE(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(),mCurrentLocation.getSpeed(),speedCustom));
            /**
             * ENVIAR AUTOMATICAMENTE,SI COMO MINIMO HAY 7 DATOS REGISTRADOS EN LA BD LOCAL O ES EL PRIMER REGISTRO
             */
            System.out.println("Total Locations:"+totalLocations);

            LAST_TIME_READ = CURRENT_TIME;
            /**
             TODO ANTES ERA 10mn 'LAST_TIME_UPDATED' AHORA 3mn
             */
//            if(totalLocations>7||CURRENT_TIME-LAST_TIME_UPDATED>3*60*1000){
//                LAST_TIME_UPDATED = CURRENT_TIME;
//                new SendBatchLocationTmpTask(this,new CallbackRequest() {
////                new SendBatchLocationTask(this,new CallbackRequest() {
//                    @Override
//                    public void processFinish() {}
//                    @Override
//                    public void processFinish(ResponseE response) {
//                        System.out.println("Enviado.....!!!!");
//                    }
//                }).execute();
//            }
            /**
             * ACTUALIZAR LAS ULTIMAS POSICIONES
             */
            for (int i=TOTAL_PRE_LOC-1;i>0;i--)
                preLocation[i] = preLocation[i-1];
            preLocation[0] = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
            System.out.println(Arrays.deepToString(preLocation));
        }
    }
    public double velocidadAproximada(long time){
        double difDistance = 0;
        long difTime = 0;
        if(preLocation[0].latitude!=0.0){
            difTime = (time - LAST_TIME_READ)/1000;
            difDistance = calcDistance(preLocation[0].latitude, preLocation[0].longitude,mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
//            difDistance = CalculationByDistance(preLocation[0].latitude, preLocation[0].longitude,mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
        difTime = difTime==0?1:difTime;
        String difDist = String.format("%.5f", difDistance/difTime);
        difDist = difDist.replace(",",".");
        return Double.parseDouble(difDist);
//        return difDistance/difTime;
    }
    public double CalculationByDistance(double lat1, double lon1, double lat2, double lon2) {
        double Radius = 6371.00;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return Radius * c;
    }

//    public boolean isDistanceValid(double last_lat, double last_lng, double current_lat, double current_lng){
//        double minDistance = 10;
//        double distance = 6371 * (2 * Math.atan2(Math.sqrt((Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) * Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) + Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.cos((last_lat * Math.PI / 180)) * Math.cos((current_lat * Math.PI / 180)))), Math.sqrt(1-(Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) * Math.sin(((current_lat - last_lat) * Math.PI / 180)/2) + Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.sin(((current_lng - last_lng) * Math.PI / 180)/2) * Math.cos((last_lat * Math.PI / 180)) * Math.cos((current_lat * Math.PI / 180))))))*1000;
//        if(distance >= minDistance)
//            return true;
//        return false;
//    }

    public double calcDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;
        return dist;
    }
    public double calcAngle(double latN,double lngN,double latN1,double lngN1,double latN2,double  lngN2){
        LatLng vAlfa = new LatLng((latN2 - latN1), (lngN2 - lngN1));
        LatLng vBeta = new LatLng((latN - latN1), (lngN - lngN1));
        double angle = Math.acos((vAlfa.latitude * vBeta.latitude + vAlfa.longitude * vBeta.longitude) / (Math.sqrt(Math.pow(vAlfa.latitude,2) + Math.pow(vAlfa.longitude,2)) * Math.sqrt(Math.pow(vBeta.latitude,2) + Math.pow(vBeta.longitude,2))));
        return angle*180/Math.PI;
    }

    /**
     * Selecciona una cantidad N de puntos, y determinar si ya salieron de un radio de distancia.
     * @return Lista de posiciones
     */
//    public  ArrayList<LatLng> removePeak(){
//        int NUMERO_PUNTOS = 8;    //
//        int DISTANCIA_RADIO = 10; //mtrs
//        ArrayList<LatLng> res = new ArrayList<>();
//        int idxRef = 0;
//        if(ListaPosicion.size()<NUMERO_PUNTOS){
//            res.add(ListaPosicion.get(idxRef));
//            return res;
//        }
//        while (idxRef<ListaPosicion.size()){
//            int cantMay = 0;
//            int posPrimMay = -1;
//            int totalVerificado = idxRef;
//            String distss = "";
//            for (int i=idxRef+1; i<idxRef+NUMERO_PUNTOS && i<ListaPosicion.size(); i++){
//                double dist = calcDistance(ListaPosicion.get(idxRef).latitude,ListaPosicion.get(idxRef).longitude,ListaPosicion.get(i).latitude,ListaPosicion.get(i).longitude);
//                distss +="("+dist+")";
//                if(dist>DISTANCIA_RADIO){
//                    if(posPrimMay==-1)posPrimMay = i;
//                    cantMay++;
//                }
//                totalVerificado=i;
//            }
//            System.out.println(distss+" |||:"+(totalVerificado-posPrimMay+1)+"  -  "+cantMay+"\n");
//            if(totalVerificado-posPrimMay+1==cantMay){//Si sale del radio
//                res.add(ListaPosicion.get(idxRef));
//                idxRef = posPrimMay;
//            }else{
//                idxRef++;
//            }
//        }
//        System.out.println("----------------------------TOT:"+res.size());
//        return res;
//    }

    public ArrayList<LatLng> removePeak(){
        HARDATA = false;
        if(HARDATA){
            ListaPosicion = new ArrayList<>();
            try {
                JSONArray ja = new JSONArray(new Util(this).getJS());
                for (int i=0; i<ja.length(); i++){
                    JSONObject job = ja.getJSONObject(i);
                    ListaPosicion.add(new LatLng(job.getDouble("lat"),job.getDouble("lng")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        double sumaCorregidos = 0;
        LatLng[] pox2 = new LatLng[ListaPosicion.size()];
        int id = 0;
        for (LatLng ll : ListaPosicion){
            pox2[id++]=ll;
        }

//        LatLng[] pox2 = new LatLng[]{new LatLng(-12.0884761,-77.0166139),new LatLng(-12.0884868,-77.0166395),new LatLng(-12.0884631,-77.0165873),new LatLng(-12.0884966,-77.016337),new LatLng(-12.0884515,-77.0165978),new LatLng(-12.0884807,-77.0166229),new LatLng(-12.0884961,-77.0166529),new LatLng(-12.0885334,-77.0161978),new LatLng(-12.0884348,-77.0165477),new LatLng(-12.0884067,-77.0164902),new LatLng(-12.0883968,-77.0162291),new LatLng(-12.0884296,-77.0165992),new LatLng(-12.0884391,-77.0165693),new LatLng(-12.0884296,-77.0165751),new LatLng(-12.0884621,-77.0165983),new LatLng(-12.0884326,-77.0165897),new LatLng(-12.0884554,-77.016615),new LatLng(-12.0884667,-77.0166246),new LatLng(-12.0884547,-77.016599),new LatLng(-12.0886405,-77.0165961),new LatLng(-12.0884504,-77.0164486),new LatLng(-12.0884373,-77.0166008),new LatLng(-12.0884176,-77.0165704),new LatLng(-12.0884643,-77.0165829),new LatLng(-12.0884681,-77.0165969),new LatLng(-12.0884158,-77.0165634),new LatLng(-12.088485,-77.0166317),new LatLng(-12.0884522,-77.0166014),new LatLng(-12.0884983,-77.0162965),new LatLng(-12.0885375,-77.0163119),new LatLng(-12.0884547,-77.0165887),new LatLng(-12.0884427,-77.0165702),new LatLng(-12.0885258,-77.0162734),new LatLng(-12.0885379,-77.0163192),new LatLng(-12.0884844,-77.0166584),new LatLng(-12.0884567,-77.0165635),new LatLng(-12.0884828,-77.0166057),new LatLng(-12.0884445,-77.0163576),new LatLng(-12.0884916,-77.0163074),new LatLng(-12.0884352,-77.0165923),new LatLng(-12.088446,-77.0165583),new LatLng(-12.0884576,-77.0162139),new LatLng(-12.0884498,-77.01624),new LatLng(-12.0884816,-77.0165871),new LatLng(-12.0884297,-77.0165853),new LatLng(-12.0884433,-77.0165971),new LatLng(-12.0884559,-77.0162832),new LatLng(-12.0884684,-77.0162866),new LatLng(-12.0884424,-77.0163145),new LatLng(-12.0884491,-77.0165829),new LatLng(-12.0884462,-77.0166031),new LatLng(-12.0884984,-77.0166557),new LatLng(-12.088483,-77.0166607),new LatLng(-12.0884873,-77.0166016)};

        ArrayList<LatLng> res = new ArrayList<>();
        System.out.println("-------------------------------------");
        for (int i=1; i<pox2.length-1; i++){
            double ddPS = calcDistance(pox2[i-1].latitude, pox2[i-1].longitude, pox2[i].latitude, pox2[i].longitude);
            double ddST = calcDistance(pox2[i].latitude, pox2[i].longitude, pox2[i+1].latitude, pox2[i+1].longitude);
            double ddPT = calcDistance(pox2[i-1].latitude, pox2[i-1].longitude, pox2[i+1].latitude, pox2[i+1].longitude);
//            double ddAngle = calcAngle(pox2[i - 1].latitude, pox2[i - 1].longitude, pox2[i].latitude, pox2[i].longitude, pox2[i+1].latitude, pox2[i+1].longitude);


//            System.out.println("Add->>"+(pox2[i-1].latitude +","+ pox2[i-1].longitude));

            double cd = 0;
            int sz = res.size();
            if(res.size()==0||res.size()==1){
                sz = 1;
                sumaCorregidos = 0;
            }else{
                cd = calcDistance(res.get(sz-2).latitude,res.get(sz-2).longitude,res.get(sz-1).latitude,res.get(sz-1).longitude);
//                System.out.println("Two dis :"+cd);
                sumaCorregidos += cd;
            }
//            System.out.println("DIS  A:"+ddPS+"   B:"+ddST+"   C:"+ddPT+ "  Angle:"+ddAngle+"  Pr:"+(sumaCorregidos/(double)sz));

//            System.out.println("DIS  A:"+ddPS+"   B:"+ddST+"   C:"+ddPT+ "  Pr:"+(sumaCorregidos/(double)sz) + "  DST:"+cd);
            double promedio = sumaCorregidos/(double)sz;
            res.add(new LatLng(pox2[i - 1].latitude, pox2[i - 1].longitude));
//            if(ddPS>400||ddST>400){
//                System.out.println("("+ddPS+"|"+ddST+"|"+ddPT+")"+" Paso de 400-->("+pox2[i-1].latitude +","+ pox2[i-1].longitude+") ("+pox2[i].latitude +","+ pox2[i].longitude+") ("+pox2[i].latitude +","+ pox2[i].longitude+")");
//            }

//            if((ddPT<ddPS&&ddPT<ddST&&ddPT<25) || (ddPS>500&&ddST>500)  || ddPS>promedio*2){// Es Pico
//            if((ddPT<ddPS&&ddPT<ddST) || (ddPS>500&&ddST>500)){// Es Pico

            if((ddPT<ddPS&&ddPT<ddST) || (ddPS>500&&ddST>500) || (ddPT>MIN_DISTANCE_DISPLACEMENT||ddPT>MIN_DISTANCE_DISPLACEMENT)){// Es Pico
//                double angle = calcAngle(pox2[0].latitude, pox2[0].longitude, pox2[1].latitude, pox2[1].longitude, pox2[2].latitude, pox2[2].longitude);
                i++;
//                System.out.println("Salto de Pico en "+i);
            }
//            System.out.println("-------------------------------------");
        }
        if (ListaPosicion.size()<=2)return ListaPosicion;
        System.out.println(ListaPosicion.size()+" --SizeSubTot-- "+res.size());
        System.out.println("-");
        ArrayList <LatLng> fixPos = new ArrayList<>();
        fixPos.add(res.get(0));
        for (int i=1; i<res.size(); i++){
            LatLng a = res.get(i-1);
            LatLng b = res.get(i);
            double dist = calcDistance(a.latitude, a.longitude, b.latitude, b.longitude);
            if(dist>MIN_DISTANCE_DISPLACEMENT){
//                System.out.println("La distancia es:::"+dist+" :rem");
//                res.remove(i);
            }else {
//                System.out.println("La distancia es:::"+dist);
                fixPos.add(res.get(i));
            }
        }
        System.out.println("--------------------           ---------------");
        res = fixPos;
        fixPos = new ArrayList<>();
        fixPos.add(res.get(0));
        for (int i=1; i<res.size(); i++){
            LatLng a = res.get(i-1);
            LatLng b = res.get(i);
            double dist = calcDistance(a.latitude, a.longitude, b.latitude, b.longitude);
            if(dist>MIN_DISTANCE_DISPLACEMENT){
                System.out.println("La distancia es:::"+dist+" :rem");
//                res.remove(i);
            }else {
                System.out.println("La distancia es:::"+dist);
                fixPos.add(res.get(i));
            }
        }
        System.out.println(ListaPosicion.size()+" --SizeTot-- "+res.size() + " --- SZ:"+fixPos.size());
        return fixPos;
    }

//    public ArrayList<LatLng> fixTriangle(int minDistance){
//
////        ListaPosicion = new ArrayList<>();
////        try {
////            JSONArray ja = new JSONArray(new Util(this).getJS());
////            for (int i=0; i<ja.length(); i++){
////                JSONObject job = ja.getJSONObject(i);
////                ListaPosicion.add(new LatLng(job.getDouble("lat"),job.getDouble("lng")));
////            }
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
//
//        LatLng pox[] = new LatLng[ListaPosicion.size()];
//        int id=0;
//        String rr = "";
//        for (LatLng ll : ListaPosicion){
//            pox[id++]=ll;
//            rr += "new LatLng("+ll.latitude+","+ll.longitude+"),";
//        }
////        System.out.println("El Arr era:"+rr);
//        System.out.println("El Size era:"+pox.length);
//
//
////        LatLng pox[] = {new LatLng(-12.0884713,-77.0166214),new LatLng(-12.0884713,-77.0166214),new LatLng(-12.0884138,-77.0164545),new LatLng(-12.0884296,-77.0163989),new LatLng(-12.088415,-77.0163791),
////                      new LatLng(-12.0883538,-77.0165635),new LatLng(-12.0884921,-77.0163177),new LatLng(-12.088299,-77.0163112),new LatLng(-12.0879716,-77.0160361),new LatLng(-12.0876123,-77.0157877),new LatLng(-12.0869249,-77.0155599),
////                      new LatLng(-12.0864589,-77.015667),new LatLng(-12.0868714,-77.0157994),new LatLng(-12.0871849,-77.0161366),new LatLng(-12.0877569,-77.0160454),new LatLng(-12.0880412,-77.0164459),new LatLng(-12.0883416,-77.0164068),
////                      new LatLng(-12.0884702,-77.016662),new LatLng(-12.0884609,-77.0162953),
////                      new LatLng(-12.0884175,-77.0165631)};
//
//
////        LatLng[] pox2 = new LatLng[]{new LatLng(-12.0884742,-77.0165966),new LatLng(-12.0884742,-77.0165966),new LatLng(-12.0887081,-77.0160139),new LatLng(-12.0884379,-77.0166053)};
////        LatLng[] pox2 = new LatLng[]{new LatLng(-12.0884742,-77.0165966),new LatLng(-12.0884742,-77.0165966),new LatLng(-12.0887081,-77.0160139),new LatLng(-12.0884379,-77.0166053),new LatLng(-12.0884704,-77.0166353)};
////        double angle2 = calcAngle(pox2[0].latitude, pox2[0].longitude, pox2[1].latitude, pox2[1].longitude, pox2[2].latitude, pox2[2].longitude);
////        double disss = calcDistance(pox2[0].latitude, pox2[0].longitude, pox2[2].latitude, pox2[2].longitude);
////        System.out.println("ANGLE ES:"+angle2+"  :DIST"+disss);
////        angle2 = calcAngle(pox2[1].latitude, pox2[1].longitude, pox2[2].latitude, pox2[2].longitude,pox2[3].latitude, pox2[3].longitude);
////        disss = calcDistance(pox2[1].latitude, pox2[1].longitude, pox2[3].latitude, pox2[3].longitude);
////        System.out.println("ANGLE ES:"+angle2+"  -*- DIST:"+disss);
////        ListaPosicion = new ArrayList<>();
////        for (int i=0; i<pox2.length;i++)ListaPosicion.add(new LatLng(pox2[i].latitude,pox2[i].longitude));
//
//        int tam = pox.length;
//        ArrayList<LatLng> res = new ArrayList<>();
//        LatLng ce;
//        LatLng le;
//        double dis;
//        for (int i=0; i<tam; i++) {
//            ce = pox[i];
//            if(i==0) {
//                res.add(pox[i]);
//            }else{
//                le = res.get(res.size()-1);
//                dis = calcDistance(le.latitude, le.longitude, ce.latitude, ce.longitude);
//                System.out.println("**--**"+dis);
//                if(res.size()>=3) {
//                    LatLng leSub = res.get(res.size()-2);
//                    double angle = calcAngle(ce.latitude, ce.longitude, le.latitude, le.longitude, leSub.latitude, leSub.longitude);
//                    System.out.println(ce.latitude+","+ce.longitude+" - "+le.latitude +","+ le.longitude+" - "+leSub.latitude +" - "+ leSub.longitude);
//                    System.out.println("El o La Dis es:"+dis);
//                    System.out.println("El Angulo es:"+angle);
//                    if(dis >= minDistance){
//                        if(angle<=20) {
//                                res.remove(res.size()-1);
//                        }else {
//                            res.add(pox[i]);
//                        }
//                    }
//                }else{
//                    if(dis >= minDistance) {
//                        res.add(pox[i]);
//                    }
//                }
//            }
//        }
//        if(res.size()==3)res.remove(1);
//        System.out.println("El Size es:"+res.size());
//        for (LatLng ll : res) System.out.println(ll.latitude+",,"+ll.longitude+" - ");
//        res = new ArrayList<>();
////        res.add(new LatLng());
//        return res;
//    }
}