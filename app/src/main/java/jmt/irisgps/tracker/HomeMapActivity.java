package jmt.irisgps.tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import jmt.irisgps.tracker.dialog.MessageDialog;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.data_access.TasksDA;
import jmt.irisgps.tracker.dialog.RegisterIncidentDialog;
import jmt.irisgps.tracker.interfaz.CallbackDialog;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.CustomerE;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.service.LocationFusedService;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class HomeMapActivity extends ActionBarActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{
    private GoogleMap map;
    Toolbar toolbar;
    Util util;
    Marker marker[];
    Marker markerME;
    HashMap<String,Integer> HashCli;
    Intent intentGPS,intentFused;
    TasksDA tasksDA;
    List<TasksE> taskList;
    GpsReceiver myReceiver;
    PolylineOptions OptionsLineGreen;
    Polyline lineGreen;
    Jmstore jmstore;
    int REQUEST_CODE = 10101;
    LatLng ME;
    double LAST_LAT=0,LAST_LNG=0;
    AccessAccountE vehicle;
    Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_bar);
        util = new Util(this);
        jmstore = new Jmstore(this);
        try {
            LAST_LAT=Double.parseDouble(jmstore.get(S.POSITION.LAST_LAT));
            LAST_LNG=Double.parseDouble(jmstore.get(S.POSITION.LAST_LNG));
        }catch (Exception e){
            LAST_LAT = 0;
            LAST_LNG = 0;
        }
        gson = new Gson();
        tasksDA = new TasksDA();
        vehicle = new UserAccountDA().select();
        getSupportActionBar().setTitle(vehicle.getPlate());
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        ME = new LatLng(LAST_LAT,LAST_LNG);
        if(LAST_LAT!=0&&LAST_LNG!=0){
            MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
            markerME = map.addMarker(mo);
        }
        addCustomerPlaces(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        intentFused = new Intent(this, LocationFusedService.class);
        startService(intentFused);
        myReceiver = new GpsReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LocationFusedService.NEW_GPS);
        registerReceiver(myReceiver, intentFilter);
        turnGPSOn();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addCustomerPlaces(false);
    }

    private void addCustomerPlaces(boolean first_time){
        final LatLngBounds.Builder builderLL = new LatLngBounds.Builder();
        HashCli = new HashMap<>();
        taskList = tasksDA.select();
        if(marker!=null)for (int i=0;i<marker.length;i++)marker[i].remove();
        marker = new Marker[taskList.size()];
        boolean IS_FINISH=true;
        for (int i=0;i<taskList.size();i++){
            long task_st_id = taskList.get(i).getTask_state_id();
            if(task_st_id==S.TASK_STATE.PROGRAMADA||task_st_id==S.TASK_STATE.POSPUESTA)
                IS_FINISH = false;
            CustomerE cli = taskList.get(i).getCustomer();
            int ic_loc = 0;
            int task_state = (int)taskList.get(i).getTask_state_id();
            if(task_state==S.TASK_STATE.PROGRAMADA)
                ic_loc = R.drawable.ic_place_purple;
            else if(task_state==S.TASK_STATE.REALIZADA)
                ic_loc = R.drawable.ic_place_green;
            else if(task_state==S.TASK_STATE.POSPUESTA)
                ic_loc = R.drawable.ic_place_orange;
            else if(task_state==S.TASK_STATE.CANCELADA)
                ic_loc = R.drawable.ic_place_gray;
            else
                ic_loc = R.drawable.ic_pistachio;
            LatLng ll = new LatLng(cli.getLat(),cli.getLng());
            MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory
                    .fromResource(ic_loc)).position(ll).title(cli.getName());
            marker[i] = map.addMarker(mo);
            HashCli.put(marker[i].getId(),i);
            builderLL.include(ll);
        }
        if(first_time&&taskList.size()>0)
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition arg0) {
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builderLL.build(), 100));
                    map.setOnCameraChangeListener(null);
                }
            });
        AccessAccountE user = new UserAccountDA().select();
        user.setRoutes_completed(IS_FINISH);
        user.save();
        map.setOnMarkerClickListener(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_registro_incidente) {
            registroIncidente();
            return true;
        }
        if (id == R.id.action_desconectar) {
            util.finalizarSesion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void registroIncidente() {
        new RegisterIncidentDialog(this).show(this,ME,new CallbackDialog() {
            @Override
            public void finish() {}
        });
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        if(HashCli.containsKey(marker.getId())){
            Intent it= new Intent(this,TaskCustomerActivity.class);
            TasksE task = taskList.get(HashCli.get(marker.getId()));
            it.putExtra(S.TASK.id,task.getId());
            it.putExtra(S.TASK.route_task_id,task.getTask_id());
            startActivity(it);
        }
        return false;
    }

    String rr = "";
    private class GpsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            boolean SHOWLINE = true;
            Bundle extra = arg1.getExtras();
            if(extra!=null){
                int type_msg = extra.getInt(S.TYPE_MSG.type_msg);
                if(type_msg==S.TYPE_MSG.FIRST_POSITION){
                    System.out.println("Reload Position...!!!");
                    Double Lat = extra.getDouble(S.POSITION.latitude);
                    Double Lng = extra.getDouble(S.POSITION.longitude);
                    boolean isFirst = ME.latitude == LAST_LAT || ME.longitude == LAST_LNG;
                    if(isFirst){
                        ME = new LatLng(Lat,Lng);
                        if(markerME!=null) markerME.remove();
                        MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
                        markerME = map.addMarker(mo);
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, 18));
                        map.animateCamera(CameraUpdateFactory.zoomTo(18), 850, null);
                    }
                }else if(type_msg==S.TYPE_MSG.ADD_POSITION){
                    double Lat = extra.getDouble(S.POSITION.latitude);
                    double Lng = extra.getDouble(S.POSITION.longitude);
                    jmstore.push(S.POSITION.LAST_LAT, String.valueOf(Lat));
                    jmstore.push(S.POSITION.LAST_LNG, String.valueOf(Lng));
                    if(SHOWLINE){
                        float speed = extra.getFloat("speed");
                        float accuracy = extra.getFloat("accuracy");
                        double speedKM = extra.getDouble("speedKM");
                        String provider = extra.getString("provider");
                        String moving = extra.getString("moving");
                        String tit = Lat+","+Lng;
                        ((TextView)findViewById(R.id.txtLL)).setText(tit);
                        ((TextView)findViewById(R.id.txtLL3)).setText(moving+"  |  "+speed+" | "+speedKM);

                        if(extra.containsKey("sin_fix")){
                            String sin_fix = extra.getString("sin_fix");
                            ArrayList<LatLng> locsG= gson.fromJson(sin_fix, new TypeToken<ArrayList<LatLng>>(){}.getType());
                            OptionsLineGreen = new PolylineOptions().width(5).color(Color.GREEN).geodesic(true);
                            if(lineGreen!=null) lineGreen.remove();
                            for (LatLng ll : locsG)
                                OptionsLineGreen.add(ll);
                            lineGreen = map.addPolyline(OptionsLineGreen);
                        }
                    }
                    ME =  new LatLng(Lat,Lng);
                    if(markerME!=null) markerME.remove();
                    MarkerOptions mo = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_blue)).position(ME);
                    markerME = map.addMarker(mo);
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(ME, map.getCameraPosition().zoom));
//                    map.animateCamera(CameraUpdateFactory.zoomTo(map.getCameraPosition().zoom), 1250, null);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(ME)
                                    .zoom(map.getCameraPosition().zoom)
                                    .build();
                            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1250, null);
                        }
                    }, 750);
                }
            }
        }
    }

    private void alertTurnGPS(){
        new MessageDialog(HomeMapActivity.this).show(getString(R.string.s_activar_gps), new CallbackDialog() {
            @Override
            public void finish() {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }
    private void turnGPSOn(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")){
            alertTurnGPS();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE && resultCode == 0){
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
                if(intentGPS!=null)
                startService(intentGPS);
            }else{
                alertTurnGPS();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
    }
}
// http://movistar.irisgps.com/apk/iris-gps-movistar.apk