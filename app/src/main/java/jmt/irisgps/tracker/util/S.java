package jmt.irisgps.tracker.util;

import java.util.HashMap;

/**
 * Created by JMTech-Android on 31/03/2015.
 */
public final class S {
    public static final String TOKEN = "token";
    public static final String GCMID = "gcm";
    public static final String mobile = "mobile";
    public static final String IMEI = "IMEI";
    public static final String Cookie = "Cookie";
    public static final String UserAgent = "User-Agent";
    public static final String UserAgentVal = "androidjmhttp";
    public static final String Set_Cookie = "Set-Cookie";
    public static final String Device_Type = "Device-Type";
    public static final String Device_TypeVal = "ws";
    public static final String CONTACT_OPEN = "CONTACT_OPEN";
    public static final class GOOGLE {
        public static final String PROJECT_ID = "424007367655";
    }
    public static final class ACCESS_ACCOUNT {
        public static final String id = "id";
        public static final String vehicle_id = "vehicle_id";
        public static final String access_id = "access_id";
    }
    public static final class TASK {
        public static final String id = "task_id";
        public static final String route_task_id = "route_task_id";
        public static final String task_state_id = "task_state_id";
        public static final String description = "description";
    }
    public static final class TASK_STATE {
        public static final String TaskStateId = "state_id";
        public static final String CommentTaskState = "comment_state";
        public static final String Programada = "Programada";
        public static final String Realizada = "Realizada";
        public static final String Pospuesta = "Pospuesta";
        public static final String Cancelada = "Cancelada";
        public static final String Reprogramar = "Reprogramar";
        public static final int PROGRAMADA = 1;
        public static final int REALIZADA = 2;
        public static final int POSPUESTA = 3;
        public static final int CANCELADA = 4;
        public static final int REPROGRAMAR = 5;
    }
    public static final class POSITION {
        public static final String route_position_id = "route_position_id";
        public static final String latitude = "latitude";
        public static final String longitude = "longitude";
        public static final String plate = "plate";
        public static final String route_id = "route_id";
        public static final String locations = "locations";
        public static final String LAST_LAT = "LAST_LAT";
        public static final String LAST_LNG = "LAST_LNG";
    }
    public static final class RESULT {
        public static final String tasks = "tasks";
    }
    public static final class TYPE_MSG {
        public static final String type_msg = "type_msg";
        public static final String type_from = "type_from";
        public static final int ADD_POSITION = 100;
        public static final int FIRST_POSITION = 101;
        public static final int NEED_GPS_ACTIVE = 200;
    }
    public static final class RESPONSE {
        public static final String irs_without_connection = "irs000";//error Sin Conexion!
        public static final String irs_success = "irs001";//success	Respuesta para solicitudes exitosas. Toda solicitud realizada de manera correcta deberá de retornar este código
        public static final String irs401 = "irs401";//error	No se tiene Autorización para realizar la solicitud. Token inválido, número no autorizado, número no activado.
        public static final String irs_invalid_data = "irs402";//error	La información enviada no es válida o está incompleta.
        public static final String irs2001 = "irs501";//error	El servidor no pudo procesar la solicitud.
    }
}