package jmt.irisgps.tracker.interfaz;

import jmt.irisgps.tracker.model.entity.ResponseE;

public interface CallbackRequest {
    void processFinish();
    void processFinish(ResponseE response);
}