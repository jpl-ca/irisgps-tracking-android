package jmt.irisgps.tracker.interfaz;

import jmt.irisgps.tracker.model.entity.ResponseE;

public interface CallbackRequestInfo {
    void processFinish(boolean b);
    void processFinish(ResponseE response);
}