package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;
import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class StartConnectionTask extends AsyncTask<String, String, ResponseE>{
    Activity ctx;
    CallbackRequest delegate;
    public StartConnectionTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject(params[0]);
            Z = new OkHttp(ctx).makePostRequest("/ws/vehicles/auth/pair-device",jo);
            System.out.println("-----|>");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        if(response.isSuccess()){
            delegate.processFinish(response);
        }else{
            Toast.makeText(ctx, response.getMassege(), Toast.LENGTH_LONG).show();
            delegate.processFinish();
        }
    }
}