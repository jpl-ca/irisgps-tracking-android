package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.entity.ResponseE;

public class FinishConnectionTask extends AsyncTask<String, String, ResponseE>{
    Activity ctx;
    public FinishConnectionTask(Activity ctx){
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            Z = new OkHttp(ctx).makePostRequest("/ws/vehicles/auth/unpair-device",jo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
}