package jmt.irisgps.tracker.http_service;

import android.content.Context;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import jmt.irisgps.tracker.R;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class OkHttp {
//    public static String URL="http://jmwebserver.ddns.net";
//    public static String URL="http://claro.irisgps.com";
    public static String URL="http://movistar.irisgps.com";
    Context ctx;
    Jmstore js;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient client = new OkHttpClient();
    public OkHttp(Context ctx){
        this.ctx = ctx;
        js = new Jmstore(ctx);
    }
    public Response makeGetSynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        Response r = client.newCall(request).execute();
        return r;
    }
    public void makeGetASynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        client.newCall(request).enqueue(new Callback(){
            @Override
            public void onFailure(Request request, IOException e) {}
            @Override public void onResponse(Response response) throws IOException {}
        });
    }
    Response makePostRequest(String url, String json) throws IOException {
        RequestBody body2 =  RequestBody.create(JSON, json);
        Request request = prepare_request(url).post(body2).build();
        return client.newCall(request).execute();
    }
    public Request.Builder prepare_request(String url){
        String gcm_id = js.get(S.GCMID);
//        System.out.println("------> Send Header!!!");
//        System.out.println(gcm_id);
//        System.out.println(js.get(S.UserAgentVal));
//        System.out.println(js.get(S.mobile));
//        System.out.println(js.get(S.IMEI));
//        System.out.println(js.get(S.TOKEN));
//        System.out.println("------> Sent Header!!!");
        return new Request.Builder()
            .url(url)
            .addHeader(S.Cookie, js.get(S.Cookie))
            .addHeader(S.UserAgent, S.UserAgentVal)
            .addHeader(S.TOKEN, js.get(S.TOKEN))
            .addHeader(S.mobile, js.get(S.mobile))
            .addHeader(S.IMEI, js.get(S.IMEI))
            .addHeader(S.Device_Type, S.Device_TypeVal)
            .addHeader(S.GCMID, gcm_id);
    }

    public ResponseE showResponse(Response response) {
        ResponseE ss = new ResponseE();
        try {
            String in = response.body().string();
            new Util(ctx).saveFile(in);
            System.out.println("------>>>>");
            System.out.println(in);
            ss.setSuccess(response.isSuccessful());
            if(!response.isSuccessful()){
                ss.setMassege("Error del servidor");
            }else{
                JSONObject jo = new JSONObject(in);
                String code = jo.getString("code");
                ss.setCode(code);
                if(code.equals(S.RESPONSE.irs401)||code.equals(S.RESPONSE.irs_invalid_data)||code.equals(S.RESPONSE.irs2001)){
                    ss.setSuccess(false);
                }
                ss.setData(jo.getString("data"));
                ss.setMassege(jo.getString("message"));
            }
            Headers responseHeaders = response.headers();
            String ck = "";
            for (int i = 0; i < responseHeaders.size(); i++){
//                System.out.println("000:("+responseHeaders.name(i)+")="+responseHeaders.value(i));
                if(responseHeaders.name(i).equals(S.TOKEN)){
                    js.push(S.TOKEN, responseHeaders.value(i));
                }
                if(responseHeaders.name(i).equals(S.Set_Cookie)){
                    ck+=responseHeaders.value(i)+";";
                }
            }
            js.push(S.Cookie, ck);
        } catch (IOException ex) {
            ss.setMassege("No hay data");
            ss.setSuccess(false);
            return ss;
        }catch (JSONException e) {
            e.printStackTrace();
            ss.setMassege("No es JSON");
            ss.setSuccess(false);return ss;
        }
        return ss;
    }

    /**
     * @deprecated Usar showResponse Method
     */
    public ResponseE showResponse2(Response response) {
        ResponseE ss = new ResponseE();
        try {
            String in = response.body().string();
            System.out.println("------>>>>");
            new Util(ctx).saveFile(in);
            System.out.println(in);
            ss.setSuccess(response.isSuccessful());
            if(!response.isSuccessful()){
                ss.setMassege("Error del servidor");
                return ss;
            }
            JSONObject jo = new JSONObject(in);
            String code = jo.getString("code");
            ss.setCode(code);
            if(code.equals(S.RESPONSE.irs401)){
                System.out.println("A cerrar sesion....!!!");
//                ss.setSuccess(false);
                //TODO Usar en caso sea necesario cerrar sesion emergente.
//                new Util(ctx).toLogin();
                return ss;
            }
            ss.setData(jo.getString("data"));
            ss.setMassege(jo.getString("massege"));
            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++){
                if(responseHeaders.name(i).equals(S.TOKEN)){
                    new Jmstore(ctx).push(S.TOKEN, responseHeaders.value(i));
                }
            }
        } catch (IOException ex) {
            ss.setMassege("No hay data");
            ss.setSuccess(false);return ss;}
        catch (JSONException e) {
            ss.setMassege("No es JSON");
            ss.setSuccess(false);return ss;}
        return ss;
    }
    public ResponseE makeGetRequest(String service,boolean ServerExtern) {
        String resp = "{}";
        if(!ServerExtern)
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makeGetSynchronous(service);
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.irs_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
    public ResponseE makePostRequest(String service,JSONObject params) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makePostRequest(service,params.toString());
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.irs_without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
}