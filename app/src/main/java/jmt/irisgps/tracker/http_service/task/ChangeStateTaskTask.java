package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class ChangeStateTaskTask extends AsyncTask<String, String, ResponseE>{
    Activity ctx;
    CallbackRequest delegate;
    public ChangeStateTaskTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try{
            JSONObject jo = new JSONObject();
            jo.put(S.TASK.route_task_id,params[0]);
            jo.put(S.TASK.task_state_id,params[1]);
            jo.put(S.TASK.description,params[2]);
            System.out.println("Enviando "+jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/ws/vehicles/tracking-route/change-route-task-state",jo);
            System.out.println("-----|>");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE result){
        if(result.isSuccess())
             delegate.processFinish(result);
        else delegate.processFinish();
    }
}