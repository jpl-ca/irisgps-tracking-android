package jmt.irisgps.tracker.http_service.task;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONObject;

import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;

public class SendBatchLocationTmpTask extends AsyncTask<String, String, ResponseE>{
    CallbackRequest delegate;
    Context ctx;
    RoutePositionDA posDA;
    public SendBatchLocationTmpTask(Context ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        posDA = new RoutePositionDA();
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        System.out.println("PREPARANDO PARA ENVIAR");
        ResponseE Z = new ResponseE();
//        String locations = posDA.selectString();
        String locations = posDA.selectJson();
        try{
            JSONObject jo = new JSONObject();
            jo.put(S.POSITION.plate,"BDE-774");
            jo.put(S.POSITION.locations,locations);
            System.out.println(jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/api/store-locations",jo);
            System.out.println("-----|>");
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE result){
        System.out.println("RES");
        System.out.println(new Gson().toJson(result));
        if(result!=null&&result.isSuccess()){
            posDA.clear();
            delegate.processFinish(result);
        }
        else delegate.processFinish();
    }
}