package jmt.irisgps.tracker.http_service.task;

import android.content.Context;
import android.os.AsyncTask;
import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.data_access.RoutePositionDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class SendBatchLocationTask extends AsyncTask<String, String, ResponseE>{
    CallbackRequest delegate;
    Context ctx;
    RoutePositionDA posDA;
    public SendBatchLocationTask(Context ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        posDA = new RoutePositionDA();
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        System.out.println("PREPARANDO PARA ENVIAR");
        ResponseE Z = new ResponseE();
        AccessAccountE account = new UserAccountDA().select();
        String placa = account.getPlate();
//        String placa = "Q0E-987";
        long vehicle = account.getVehicle_id();
        String route = String.valueOf(account.getTracking_route_id());

        String locations = "";
        locations = posDA.selectJson();
//        ArrayList<LocationHistoriesE> locationsA = Util.posCorrectionC(posDA.select(), 30);
//        locations = posDA.selectJson(locationsA);
        try{
            JSONObject jo = new JSONObject();
            jo.put(S.POSITION.plate,placa);
            jo.put(S.POSITION.locations,locations);
            System.out.println("THE JSON IS:");
            System.out.println(jo.toString());
            Z = new OkHttp(ctx).makePostRequest("/ws/vehicles/tracking-route/store-locations",jo);
//            System.out.println("-----|>");
//            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE result){
        if(result!=null&&result.isSuccess()){
            posDA.clear();
            delegate.processFinish(result);
        }
        else delegate.processFinish();
    }
}