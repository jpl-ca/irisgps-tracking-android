package jmt.irisgps.tracker.http_service.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import java.io.IOException;
import jmt.irisgps.tracker.http_service.OkHttp;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.interfaz.CallbackRequestInfo;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.model.store.Jmstore;
import jmt.irisgps.tracker.util.S;

public class LoadInfoTask extends AsyncTask<String, String, ResponseE>{
    Activity ctx;
    CallbackRequestInfo delegate;
    GoogleCloudMessaging gcm;
    String regId;
    Jmstore jmstore;
    public LoadInfoTask(Activity ctx, CallbackRequestInfo delegate){
        this.ctx = ctx;
        this.delegate = delegate;
        regId="";
        gcm = GoogleCloudMessaging.getInstance(this.ctx);
        jmstore = new Jmstore(ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        TelephonyManager tMgr = (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        String IMEI = tMgr.getDeviceId();
        if(mPhoneNumber.trim().length()>0)jmstore.push(S.mobile, mPhoneNumber);
        if(IMEI.trim().length()>0)jmstore.push(S.IMEI, IMEI);
        regId = jmstore.get(S.GCMID);

        try {
            if (gcm == null)gcm = GoogleCloudMessaging.getInstance(ctx);
            regId = gcm.register(S.GOOGLE.PROJECT_ID);
        }catch (IOException ex) {}
        try {
            Z = new OkHttp(ctx).makeGetRequest("/ws/vehicles/tracking-route/route-info", false);
            System.out.println(new Gson().toJson(Z));
        } catch (Exception e) {
        }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE result){
        System.out.println("Viendo el resultado!!!");
        System.out.println(result.isSuccess());
        if(result.isSuccess()){
            delegate.processFinish(result);
        }else{
            if(result.getCode()==S.RESPONSE.irs_without_connection){
                delegate.processFinish(false);
                Toast.makeText(ctx, result.getMassege(), Toast.LENGTH_LONG).show();
            }else
                delegate.processFinish(true);
        }
    }
}