package jmt.irisgps.tracker;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import jmt.irisgps.tracker.http_service.task.FinishConnectionTask;
import jmt.irisgps.tracker.http_service.task.LoadInfoTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.interfaz.CallbackRequestInfo;
import jmt.irisgps.tracker.model.data_access.UserAccountDA;
import jmt.irisgps.tracker.model.data_access.TasksDA;
import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class SplashActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prepareData();
//        ordenarLL();
    }

//    private void ordenarLL() {
//        ArrayList<LatLng> list = new ArrayList<>();
//        list.add(new LatLng(0,0));
//        list.add(new LatLng(3,3));
//        list.add(new LatLng(6,3));
//        list.add(new LatLng(2,2));
//        list.add(new LatLng(8,5));
//        double latX = Double.MIN_VALUE;
//        double lonX = Double.MIN_VALUE;
//        for (LatLng ll:list){
//            latX = Math.min(latX,ll.latitude);
//            lonX = Math.min(lonX,ll.longitude);
//        }
//        ArrayList<LatLng> positions = new ArrayList<>();
//        LatLng piv = new LatLng(latX,lonX);
//        for (LatLng ll:list){
//            if(ll.latitude==piv.latitude&&ll.longitude==piv.longitude)continue;
//            piv = ll;
//            positions.add(ll);
//        }
//        final LatLng point = new LatLng(latX,lonX);
//        Collections.sort(positions, new Comparator<LatLng>() {
//            @Override
//            public int compare(LatLng lhs, LatLng rhs) {
//                float angleA = getAngle(point,rhs);
//                float distA = calcDistance(point.latitude, point.longitude, rhs.latitude, rhs.longitude);
//                float angleB = getAngle(point, lhs);
//                float distB = calcDistance(point.latitude, point.longitude, lhs.latitude, lhs.longitude);
//                System.out.println("------------------------>("+rhs.latitude+","+rhs.longitude+")("+lhs.latitude+","+lhs.longitude+")  |  "+angleA+" - "+angleB+"  |  "+distA+" - "+distB);
//                if(angleA>angleB)return 1;
//                if(angleA==angleB&&distA<distB)return 1;
//                return -1;
//            }
//        });
//        System.out.println("*******************************************");
//        for (LatLng ll:positions){
////            System.out.print(ll.latitude + "," + ll.longitude+" ");
//            System.out.print(ll.longitude+ "," + ll.latitude +",0 ");
//        }
//        System.out.println("");
//    }
//    public float getAngle(LatLng A,LatLng B) {
//        float angle = (float) Math.toDegrees(Math.atan2(A.longitude - B.longitude, A.latitude- B.latitude));
//        if(angle < 0)
//            angle += 180;
//        if(angle >= 180)
//            angle = 0;
//        return angle;
//    }
//    public float calcDistance(double lat1, double lng1, double lat2, double lng2) {
//        float earthRadius = 6371000; //meters
//        float dLat =(float) Math.toRadians(lat2-lat1);
//        float dLng =(float) Math.toRadians(lng2-lng1);
//        float a =(float) (Math.sin(dLat/2) * Math.sin(dLat/2) +
//                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
//                        Math.sin(dLng/2) * Math.sin(dLng/2));
//        float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)));
//        return earthRadius * c;
//    }


    private void prepareData() {
        LoadInfoTask ltask = new LoadInfoTask(this,new CallbackRequestInfo() {
            @Override
            public void processFinish(boolean b) {
                check(b);
            }
            @Override
            public void processFinish(ResponseE response) {nextStep(response);}
        });
        ltask.execute();
    }

    /**
     *
     * @param b TRUE: There is Connection to server, FALSE: There is not Connection to server
     */
    private void check(boolean b) {
        final UserAccountDA ac = new UserAccountDA();
        AccessAccountE user = ac.select();
        if(!b&&ac.Count()>0&&user.getTracking_route_id()!=null)
            startActivity(new Intent(SplashActivity.this,HomeMapActivity.class));
        else{
            new FinishConnectionTask(this).execute();
            startActivity(new Intent(SplashActivity.this,AccessActivity.class));
        }
        finish();
    }

    private void nextStep(ResponseE response) {
        if(response.getCode().equals(S.RESPONSE.irs_success)){
            try {
                JSONObject jo = new JSONObject(response.getData());
                if(!jo.isNull(S.RESULT.tasks))
                    new TasksDA().insert(jo.getString(S.RESULT.tasks));
                else
                    new TasksDA().deleteAll();
                new UserAccountDA().updateRouteTask(jo.getString(S.ACCESS_ACCOUNT.id));
            } catch (JSONException e) {}
            startActivity(new Intent(SplashActivity.this,HomeMapActivity.class));
            finish();
        }else{
            new FinishConnectionTask(this).execute();
            startActivity(new Intent(SplashActivity.this,AccessActivity.class));
            finish();
        }
    }
}