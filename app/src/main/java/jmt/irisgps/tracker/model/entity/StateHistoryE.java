package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 21/05/2015.
 */
public class StateHistoryE extends SugarRecord<StateHistoryE> implements Serializable {
    @Expose
    @SerializedName("id")
    private long state_history_id;
    @Expose
    private long task_state_id;
    @Expose
    private String description;
    @Expose
    private long route_task_id;
//    @Expose
//    @SerializedName("state.name")
//    private String state_name;
    @Expose
    @SerializedName("created_at")
    private String fecha;

    public long getState_history_id() {
        return state_history_id;
    }

    public void setState_history_id(long state_history_id) {
        this.state_history_id = state_history_id;
    }

    public long getTask_state_id() {
        return task_state_id;
    }

    public void setTask_state_id(long task_state_id) {
        this.task_state_id = task_state_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public long getRoute_task_id() {
        return route_task_id;
    }

    public void setRoute_task_id(long route_task_id) {
        this.route_task_id = route_task_id;
    }
}