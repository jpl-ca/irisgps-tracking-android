package jmt.irisgps.tracker.model.entity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class LocationHistoriesE extends SugarRecord<LocationHistoriesE> {
    @Expose
    @SerializedName("id")
    private long location_id;
    @Expose
    private long vehicle_id;
    @Expose
    private String tracking_route_id;
    @Expose
    private double lat;
    @Expose
    private double lng;
    @Expose
    private long incident_type_id;
    @Expose
    private String incident_description;
    @Expose
    private String created_at;
    @Expose
    private String updated_at;
    @Expose
    private double speed_device;
    @Expose
    private double speed_custom;
    @Expose
    private int movement;
    @Expose
    private int contact_open;
    public LocationHistoriesE(){}
    public LocationHistoriesE(double lat,double lng,double speed_device,double speed_custom){
        this.lat = lat;
        this.lng = lng;
        this.speed_device = speed_device;
        this.speed_custom = speed_custom;
    }
    public LocationHistoriesE(double lat,double lng,double speed_device,double speed_custom,int movement,int contact_open){
        this.lat = lat;
        this.lng = lng;
        this.speed_device = speed_device;
        this.speed_custom = speed_custom;
        this.movement = movement;
        this.contact_open = contact_open;
    }
    public LocationHistoriesE(double lat,double lng,long incident_type_id,String incident_description){
        this.lat = lat;
        this.lng = lng;
        this.incident_type_id = incident_type_id;
        this.incident_description = incident_description;
    }

    public long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(long location_id) {
        this.location_id = location_id;
    }

    public long getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(long vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getTracking_route_id() {
        return tracking_route_id;
    }

    public void setTracking_route_id(String tracking_route_id) {
        this.tracking_route_id = tracking_route_id;
    }

    public double getLatitud() {
        return lat;
    }

    public void setLatitud(double lat) {
        this.lat = lat;
    }

    public double getLongitud() {
        return lng;
    }

    public void setLongitud(double lng) {
        this.lng = lng;
    }

    public long getIncident_type_id() {
        return incident_type_id;
    }

    public void setIncident_type_id(long incident_type_id) {
        this.incident_type_id = incident_type_id;
    }

    public String getIncident_description() {
        return incident_description;
    }

    public void setIncident_description(String incident_description) {
        this.incident_description = incident_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getSpeed_device() {
        return speed_device;
    }

    public void setSpeed_device(double speed_device) {
        this.speed_device = speed_device;
    }

    public double getSpeed_custom() {
        return speed_custom;
    }

    public void setSpeed_custom(double speed_custom) {
        this.speed_custom = speed_custom;
    }

    public int getMovement() {
        return movement;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public int getContact_open() {
        return contact_open;
    }

    public void setContact_open(int contact_open) {
        this.contact_open = contact_open;
    }
}