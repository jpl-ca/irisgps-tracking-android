package jmt.irisgps.tracker.model.entity;
/**
 * Created by JMTech-Android on 22/05/2015.
 */
public class ResponseE {
    boolean success;
    String code;
    String data;
    String massege;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMassege() {
        return massege;
    }

    public void setMassege(String massege) {
        this.massege = massege;
    }
}