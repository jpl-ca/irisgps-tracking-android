package jmt.irisgps.tracker.model.data_access;
import org.json.JSONArray;

import java.util.List;

import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.util.S;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class UserAccountDA {
    public void insert(AccessAccountE access_account){
        delete();
        System.out.println("Guardando AccessAccount!!!"+access_account.getTracking_route_id());
        access_account.save();
    }
//    public void update(AccessAccountE access_account){
//        long id = getID(access_account.getAccess_id());
//        access_account = AccessAccountE.findById(AccessAccountE.class, id);
//        access_account.save();
//    }

    /*
        Al cargar las tareas se registrara en AccessAccountE el valor de RouteTaskId
     */
    public void updateRouteTask(String id){
        AccessAccountE aa = select();
        aa.setTracking_route_id(id);
        aa.save();
    }
    public AccessAccountE select(){
        try {
            List<AccessAccountE> access_accounts = AccessAccountE.listAll(AccessAccountE.class);
            return access_accounts.get(access_accounts.size()-1);
        }catch (Exception e){
            return new AccessAccountE();
        }
    }
    public int Count(){
        return AccessAccountE.listAll(AccessAccountE.class).size();
    }
    public void delete(){
        AccessAccountE.deleteAll(AccessAccountE.class);
    }
    public long getID(long access_account_id){
        List<AccessAccountE> listU = AccessAccountE.find(AccessAccountE.class, S.ACCESS_ACCOUNT.access_id + " = ?", String.valueOf(access_account_id));
        if(listU.size()==0)return 0L;
        return listU.get(0).getId();
    }

    public void logout() {
        if(Count()>0){
            AccessAccountE user = select();
            user.setTracking_route_id(null);
            user.save();
        }
    }
}