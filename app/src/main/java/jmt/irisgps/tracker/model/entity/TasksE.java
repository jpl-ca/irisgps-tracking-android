package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jmt.irisgps.tracker.util.S;

/**
 * Created by JMTech-Android on 21/05/2015.
 */
public class TasksE extends SugarRecord<TasksE> implements Serializable{
    @Expose
    @SerializedName("id")
    private long task_id;
    @Expose
    private long task_state_id;
    @Expose
    private long tracking_route_id;
    @Expose
    private String description;
    @Expose
    @SerializedName("customer")
    private CustomerE customer;
    @Expose
    @SerializedName("state_history")
    private List<StateHistoryE> state_history;
    public TasksE(){}

    public long getTask_id() {
        return task_id;
    }

    public void setTask_id(long task_id) {
        this.task_id = task_id;
    }

    public long getTracking_route_id() {
        return tracking_route_id;
    }

    public void setTracking_route_id(long tracking_route_id) {
        this.tracking_route_id = tracking_route_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CustomerE getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerE customer) {
        this.customer = customer;
    }

    public List<StateHistoryE> getState_history() {
        return find(StateHistoryE.class, "routetaskid = ?", String.valueOf(getTask_id()));
    }

    public void setState_history(List<StateHistoryE> state_history) {
        this.state_history = state_history;
    }

    public long getTask_state_id() {
        return task_state_id;
    }

    public void setTask_state_id(long task_state_id) {
        this.task_state_id = task_state_id;
    }

    @Override
    public void save() {
        if(customer!=null)customer.save();
        if(state_history!=null)
        for (StateHistoryE sh : state_history)
            sh.save();
        super.save();
    }
}