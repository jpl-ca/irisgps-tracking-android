package jmt.irisgps.tracker.model.data_access;

import android.text.format.DateFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.List;

import jmt.irisgps.tracker.model.entity.AccessAccountE;
import jmt.irisgps.tracker.model.entity.LocationHistoriesE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.util.Util;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class RoutePositionDA {
    Gson gson;
    public RoutePositionDA(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
    }
    public int insert(LocationHistoriesE position){
        String currentTime = Util.now();
        AccessAccountE usr = new UserAccountDA().select();
        if(usr.getVehicle_id()==0){
            System.out.println("There's not Vehicle!!!");
            return Count();
        }
        position.setVehicle_id(usr.getVehicle_id());
        System.out.println("Task Completed?"+usr.isRoutes_completed());
        if(usr.isRoutes_completed()) position.setTracking_route_id(null);
        else position.setTracking_route_id(String.valueOf(usr.getTracking_route_id()));
        position.setCreated_at(currentTime);
        position.setUpdated_at(currentTime);
        position.save();

//        System.out.println("*********ADDing ROUTE********************"+currentTime);
//        System.out.println(gson.toJson(position));


//        position.setVehicle_id(6);
//        position.setTracking_route_id(null);
//        position.setCreated_at(currentTime);
//        position.setUpdated_at(currentTime);
//        position.save();
        return Count();
    }

    public int Count(){
        return LocationHistoriesE.listAll(LocationHistoriesE.class).size();
    }

    public List<LocationHistoriesE> select(){
        List<LocationHistoriesE> positions = LocationHistoriesE.listAll(LocationHistoriesE.class);
        return positions;
    }
    public LocationHistoriesE last(){
        List<LocationHistoriesE> all = select();
        if(all.size()>0){
            return all.get(all.size()-1);
        }
        return null;
    }

    public String selectJson(){
        List<LocationHistoriesE> positions = LocationHistoriesE.listAll(LocationHistoriesE.class);
        return gson.toJson(positions);
    }
    public String selectString(){
        List<LocationHistoriesE> positions = LocationHistoriesE.listAll(LocationHistoriesE.class);
        String str = "";
        for (LocationHistoriesE h : positions){
            str += h.getLatitud()+","+h.getLongitud()+" | "+h.getSpeed_device()+"|"+h.getSpeed_custom();
//            str += "<tr><td>"+h.getLatitud()+","+h.getLongitud()+"</td> <td>"+h.getSpeed_device()+"</td> <td>"+h.getSpeed_custom()+"</td> </tr>";
        }
        return str;
    }
    public String selectJson(List<LocationHistoriesE> positions){
        return gson.toJson(positions);
    }
    public void clear(){
        System.out.println("0 Locations");
        LocationHistoriesE.deleteAll(LocationHistoriesE.class);
    }
}