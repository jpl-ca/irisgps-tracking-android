package jmt.irisgps.tracker.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
/**
 * Created by JMTech-Android on 21/05/2015.
 */
public class AccessAccountE extends SugarRecord<AccessAccountE> {
//    long access_id;
    String plate;
    String dni;
    long vehicle_id;
    String tracking_route_id;
    boolean routes_completed;
    public AccessAccountE(){}
    public AccessAccountE(String plate, String dni){
        this.plate = plate;
        this.dni = dni;
        tracking_route_id = null;
    }

//    public long getAccess_id() {
//        return access_id;
//    }
//
//    public void setAccess_id(long access_id) {
//        this.access_id = access_id;
//    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public long getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(long vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getTracking_route_id() {
        return tracking_route_id;
    }

    public void setTracking_route_id(String tracking_route_id) {
        this.tracking_route_id = tracking_route_id;
    }

    public boolean isRoutes_completed() {
        return routes_completed;
    }

    public void setRoutes_completed(boolean routes_completed) {
        this.routes_completed = routes_completed;
    }
}