package jmt.irisgps.tracker.model.data_access;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jmt.irisgps.tracker.model.entity.CustomerE;
import jmt.irisgps.tracker.model.entity.StateHistoryE;
import jmt.irisgps.tracker.model.entity.TasksE;

/**
 * Created by JMTech-Android on 25/05/2015.
 */
public class TasksDA {
    Gson gson;
    public TasksDA(){
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
    public void insert(String jo){
        ArrayList<TasksE> tasks= gson.fromJson(jo, new TypeToken<ArrayList<TasksE>>(){}.getType());
        deleteAll();
        TasksE.saveInTx(tasks);
        for (TasksE t:tasks)
            insert(t);
    }
    public void insert(TasksE tasks){
        tasks.save();
    }
    public List<TasksE> select(){
        return TasksE.listAll(TasksE.class);
    }
    public int Count(){
        return TasksE.listAll(TasksE.class).size();
    }
    public TasksE selectById(long tasks_id){
        return TasksE.findById(TasksE.class, tasks_id);
    }
    public void deleteAll(){
        StateHistoryE.deleteAll(StateHistoryE.class);
        CustomerE.deleteAll(CustomerE.class);
        TasksE.deleteAll(TasksE.class);
    }
}