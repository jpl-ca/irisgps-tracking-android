package jmt.irisgps.tracker;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import jmt.irisgps.tracker.http_service.task.ChangeStateTaskTask;
import jmt.irisgps.tracker.interfaz.CallbackRequest;
import jmt.irisgps.tracker.model.data_access.TasksDA;
import jmt.irisgps.tracker.model.entity.ResponseE;
import jmt.irisgps.tracker.model.entity.StateHistoryE;
import jmt.irisgps.tracker.model.entity.TasksE;
import jmt.irisgps.tracker.util.S;
import jmt.irisgps.tracker.util.Util;

public class TaskCustomerActivity extends ActionBarActivity implements View.OnClickListener{
    Toolbar toolbar;
    Util util;
    TasksDA taskDA;
    TasksE task;
    EditText txt_comentario;
    TextView txt_name,txt_detalle,txt_direccion,txt_num_telefono;
    Button btnCall,btnEnviarEstado;
    LinearLayout lv_history_state,ll_estado_tarea;
    Spinner spinner;
    HashMap<Integer,String> hm;
    LayoutInflater inflater;
    List<StateHistoryE> SH;
    long route_task_id;
    HashMap<Integer,Integer> state_history_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_customer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.s_tarea));
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.ic_bar);
        util = new Util(this);
        Bundle extra = getIntent().getExtras();
        long id = extra.getLong(S.TASK.id);
        route_task_id = extra.getLong(S.TASK.route_task_id);
        taskDA = new TasksDA();
        task = taskDA.selectById(id);
        txt_comentario = (EditText) findViewById(R.id.txt_comentario);
        spinner = (Spinner) findViewById(R.id.sp_state);
        txt_name = (TextView)findViewById(R.id.txt_name);
        txt_detalle = (TextView)findViewById(R.id.txt_detalle);
        txt_direccion = (TextView)findViewById(R.id.txt_direccion);
        txt_num_telefono = (TextView)findViewById(R.id.txt_num_telefono);
        lv_history_state = (LinearLayout)findViewById(R.id.lv_historial_estados);
        ll_estado_tarea = (LinearLayout)findViewById(R.id.ll_estado_tarea);
        ScrollView sView = (ScrollView)findViewById(R.id.scroll);
        btnCall = (Button)findViewById(R.id.btnCall);
        btnCall.setOnClickListener(this);
        btnEnviarEstado = (Button)findViewById(R.id.btnEnviarEstado);
        btnEnviarEstado.setOnClickListener(this);
//        btnCancelarEnvio = (Button)findViewById(R.id.btnCancelarEnvio);
//        btnCancelarEnvio.setOnClickListener(this);
        sView.setVerticalScrollBarEnabled(false);
        sView.setHorizontalScrollBarEnabled(false);
        hm = Util.getStateTask();
        inflater = LayoutInflater.from(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        state_history_id = new HashMap<>();
        String[] values = new String[hm.size()-1];
        Object aSt[] = hm.keySet().toArray();
        Arrays.sort(aSt);
        for (int i=1;i<aSt.length;i++){
            values[i-1] = hm.get(Integer.parseInt(aSt[i].toString()));
            state_history_id.put(i,Integer.parseInt(aSt[i].toString()));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setVisibility(View.VISIBLE);
        txt_name.setText(task.getCustomer().getName());
        txt_direccion.setText(task.getCustomer().getAddress());
        txt_num_telefono.setText(task.getCustomer().getPhone());
        txt_detalle.setText(task.getDescription());
        SH = task.getState_history();
        showStateHistory();
    }
    public void showStateHistory(){
        if(task.getTask_state_id()==S.TASK_STATE.REALIZADA
                ||task.getTask_state_id()==S.TASK_STATE.REPROGRAMAR
                ||task.getTask_state_id()==S.TASK_STATE.CANCELADA)
            ll_estado_tarea.setVisibility(View.GONE);
        lv_history_state.removeAllViews();
        for(int i=SH.size()-1;i>=0;i--)
            addStateH(SH.get(i));
    }
    public void addStateH(StateHistoryE stateH){
        View view  = inflater.inflate(R.layout.listview_state, lv_history_state, false);
        ((TextView)view.findViewById(R.id.tv_estado)).setText(hm.get((int)stateH.getTask_state_id()));
        ((TextView)view.findViewById(R.id.tv_descripcion)).setText(stateH.getDescription());
        String fecha = stateH.getFecha().replace("/","-");
        TextView Fecha = (TextView)view.findViewById(R.id.tv_fecha);
        Fecha.setText(fecha.substring(0, fecha.length() - 3));
        try {
            Date d = Util.getDate(fecha);
            CharSequence ago = getString(R.string.s_hace_instantes);
            if(System.currentTimeMillis()-d.getTime()>1000){
                ago = DateUtils.getRelativeTimeSpanString(d.getTime(), System.currentTimeMillis(),
                        0L, DateUtils.FORMAT_ABBREV_ALL);
            }
//            Fecha.setText(String.valueOf(ago));
        } catch (ParseException e) {
            System.out.println("asd");
            e.printStackTrace();
        }
        lv_history_state.addView(view);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_us, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_desconectar) {
            util.finalizarSesion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnCall){
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + String.valueOf(task.getCustomer().getPhone())));
            startActivity(intent);
        }else if(v.getId()==R.id.btnEnviarEstado){
            final String comen = txt_comentario.getText().toString();
            final int pos = spinner.getSelectedItemPosition()+2;
            if(comen.length()>1){
                txt_comentario.setEnabled(false);
                btnEnviarEstado.setEnabled(false);
                new ChangeStateTaskTask(this,new CallbackRequest() {
                    @Override
                    public void processFinish() {
                        btnEnviarEstado.setEnabled(true);
                        txt_comentario.setEnabled(true);
                    }
                    @Override
                    public void processFinish(ResponseE response) {
                        txt_comentario.setEnabled(true);
                        btnEnviarEstado.setEnabled(true);
                        final String currentTime = Util.now();
                        StateHistoryE st = new StateHistoryE();
                        st.setTask_state_id(pos);
                        st.setFecha(currentTime);
                        st.setDescription(comen);
                        st.setRoute_task_id(route_task_id);
//                        System.out.println("*******************ESTADOSSS**********************");
//                        for(int i=SH.size()-1;i>=0;i--)
//                            System.out.println(i+"----)"+SH.get(i).getDescription());
                        SH.add(st);
//                        for(int i=SH.size()-1;i>=0;i--)
//                            System.out.println(i + "----)" + SH.get(i).getDescription());
                        ArrayList<StateHistoryE> state = new ArrayList<>();
                        state.add(st);
                        task.setState_history(state);
                        task.setTask_state_id(pos);
                        task.save();
                        txt_comentario.setText("");
                        showStateHistory();
                    }
                }).execute(String.valueOf(route_task_id),String.valueOf(pos),comen);
            }else{
                Toast.makeText(this,getString(R.string.s_complete_los_datos),Toast.LENGTH_LONG).show();
            }
        }
//        else if(v.getId()==R.id.btnCancelarEnvio){
//            txt_comentario.setText("");
//        }
    }
}